angular.module("responsive.coaches")
    .directive('forAlphaNumeric', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s ]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('disableOnClick', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.isDesabled = false;
                element.bind('click', () => {
                    var text = element[0].innerText;
                    element[0].disabled = true;
                    element[0].innerText = 'Please Wait.....';
                    $timeout(function () {
                        element[0].disabled = false;
                        element[0].innerText = text;
                    }, 3000)
                });

            }
        }
    }])
    .controller("ReserveApprovalController", ["$scope", "$http", "$window", "$element", "Coach", function ($scope, $http, $window, $element, Coach) {

        angular.extend($scope, new Coach($scope, $element));

        $scope.ReserveApproval = {};

        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            })
        }
        $scope.decServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });
        }
        $scope.updateReserveData = function (Status) {

            if(Status == 'REJECTED' && $scope.ReserveApprovalProcess.comment.$invalid){
                $scope.ReserveApprovalProcess.submitted = true;
                return;
            }else{
                $scope.ReserveApprovalProcess.submitted = false;
            }
            var input = {
                "OLDRESERVEDATA" : $scope.context.options.oldReserveData.boundObject.oldReserveData,
                "UPDATEDRESERVEDATA" : $scope.context.options.updatedReserveData.boundObject.updatedReserveData, 
                "FEATURESUMMARYID" :  $scope.context.options.FEATURESUMMARYID.boundObject.FEATURESUMMARYID,
                "FEATURENUMBER" : $scope.context.options.FEATURENUMBER.boundObject.FEATURENUMBER,
                "STATUS" : Status,
                "CLAIMNUMBER" :$scope.context.options.ClaimInfo.boundObject.ClaimInfo.CLAIMNUMBER,
                "CREATEDBY" : $scope.context.options.CREATEDBY.boundObject.CREATEDBY,
                "REJECTREASON" :$scope.ReserveApproval.COMMENT
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.decServiceCallCounter();
                    $scope.endUserTask();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    console.log(e);
                }
            };
            $scope.context.options.updateReserveData(serviceArgs);
        }
        $scope.endUserTask = function () {
            var input = {
                "taskId": $scope.context.options.TASKID.boundObject.TASKID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.decServiceCallCounter();
                    $scope.context.trigger(function () {
                        console.log("Task Ended");
                    });
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    console.log(e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.endUserTask(serviceArgs);
        }
    }]);