angular.module("responsive.coaches")



    .directive('disableOnClick', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                // scope.isDesabled = false;
                element.bind('click', () => {
                    var text = element[0].innerText;
                    element[0].disabled = true;
                    element[0].innerText = 'Please Wait.....';
                    $timeout(function () {
                        element[0].disabled = false;
                        element[0].innerText = text;
                    }, 3000)
                });
            }

        }
    }])
    .directive('forAlphaNumericCapsSpace', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });

            }
        }
    })
    .directive('forAlphaNumeric', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue1 = inputValue.replace(/[^\w\s ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlphaNumericCaps', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forEmail', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s-@.]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forDateCalendar', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^0-9-]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forDigit', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\d]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forAddress', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s\-. ,/()]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlpha', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^a-z ]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('datePicker', function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                if (attrs.whichDate && (attrs.whichDate == "past")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                } else if (attrs.whichDate && (attrs.whichDate == "future")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                else if (attrs.whichDate && (attrs.whichDate == "beforePostmortom")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else if (attrs.whichDate && (attrs.whichDate == "afterDeath")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else {

                    var parent = $(element).parent().children('.input-group-addon');

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                $(element).on("dp.change", function (e) {



                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
                $(parent).on("dp.change", function (e) {
                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
            }
        };
    })
    .directive('stringToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value, 10);
                });
            }
        };
    })
    .controller('HealthClaimController', ['$scope', '$element', '$timeout', 'Coach', '$window', function ($scope, $element, $timeout, Coach, $window) {
        "use strict";

        angular.extend($scope, new Coach($scope, $element, $timeout));

        $scope.ClaimInfo = $scope.context.options.ClaimInfo.boundObject.ClaimInfo;
        $scope.TYPE = $scope.ClaimInfo.TYPE;

        $scope.currentTeamName = $scope.context.options.TEAMNAME.boundObject.TEAMNAME;
        $scope.taskId = $scope.context.options.TASKID.boundObject.TASKID;

        // if($scope.currentTeamName== "TACDEOTEAM"){
        //     $('#claimRegistrationAnchorTag').get(0).click();
        // }else if($scope.currentTeamName== "TACHEALTHCODERTEAM"){
        //     $('#icdPcsCodingAnchorTag').get(0).click();
        // }else if($scope.currentTeamName== "TACHEALTHCOMMERCIALAPPROVALTEAM"){
        //     $('#commercialApprovalAnchorTag').get(0).click();
        // }else if($scope.currentTeamName== "TACHEALTHMEDICALAPPROVAL"){
        //     $('#medicalApprovalAnchorTag').get(0).click();
        // }else{
        //     $('#claimRegistrationAnchorTag').get(0).click();
        // }

        //initialization of variable
        $scope.providerSearch = {};
        $scope.ProviderDetails = {};
        $scope.bankSearch = {};
        $scope.BankDetails = {};
        $scope.featureClaimSearch = {};
        $scope.medicalPastHistory = [];
        $scope.NONDISCLOSURE ={};

        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            })

        };

        $scope.decServiceCallCounter = function () {

            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });

        };


        $scope.OpenClaimScreen = function () {

            var input = {
                "FEATURESUMMARYID": $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.CLAIMSUMMARY.items.length > 0)
                        $scope.claimSummary = data.CLAIMSUMMARY.items[0].indexedMap;
                    else
                        $scope.claimSummary = {};



                    $scope.decServiceCallCounter();
                    if ($scope.currentTeamName == "TACDEOTEAM" || $scope.currentTeamName == "") {
                        $('#claimRegistrationAnchorTag').get(0).click();
                    } else if ($scope.currentTeamName == "TACHEALTHCODERTEAM") {
                        $('#icdPcsCodingAnchorTag').get(0).click();
                    } else if ($scope.currentTeamName == "TACHEALTHCOMMERCIALAPPROVALTEAM") {
                        $('#commercialApprovalAnchorTag').get(0).click();
                    } else if ($scope.currentTeamName == "TACHEALTHMEDICALAPPROVAL") {
                        $('#medicalApprovalAnchorTag').get(0).click();
                    } else if ($scope.currentTeamName == "RESERVE") {
                        $('#ReserveAnchorTag').get(0).click();
                    } else {
                        $('#claimRegistrationAnchorTag').get(0).click();
                    }

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in getting Claims Details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.OpenClaimScreen(serviceArgs);


        }


        $scope.getTabData = function (TABNAME) {
            debugger;
            var input = {
                "TABNAME": TABNAME,
                "INWARDID": $scope.ClaimInfo.INWARDID,
                "FEATURESUMMARYID": $scope.ClaimInfo.FEATURESUMMARYID

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (TABNAME == "CLAIMREGISTRATIONTAB") {
                        if (data.data1.items.length > 0)
                            $scope.ProviderDetails = data.data1.items[0].indexedMap;
                        $scope.ProviderDetails.REGISTRATIONVALIDUPTO = makePickerComp($scope.ProviderDetails.REGISTRATIONVALIDUPTO);
                        if (data.data5.items.length > 0)
                            $scope.BankDetails = data.data5.items[0].indexedMap;
                        if (data.data2.items.length > 0)
                            $scope.INSURANCEHISTORYDETAILS = data.data2.items[0].indexedMap;
                        $scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT = formatDateToDatePicker($scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT);
                        if (data.data3.items.length > 0)
                            $scope.CURRENTINSURANCE = [];
                        $scope.currentAdded = [];
                        $scope.deleteCurrent = [];
                        $scope.deleteHospitalized = [];
                        for (var i = 0; i < data.data3.items.length; i++) {
                            $scope.CURRENTINSURANCE[i] = data.data3.items[i].indexedMap;
                            $scope.currentAdded[i] = true;
                        }

                        if (data.data4.items.length > 0)
                            $scope.HOSPITALIZATIONHISTORY = [];
                        $scope.hospitalizedAdded = [];
                        for (var i = 0; i < data.data4.items.length; i++) {
                            $scope.HOSPITALIZATIONHISTORY[i] = data.data4.items[i].indexedMap;
                            $scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE = formatDateToDatePicker($scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE);
                            $scope.hospitalizedAdded[i] = true;
                        }

                        if (data.data6.items.length > 0) {
                            $scope.insuredPersonData = data.data6.items[0].indexedMap;
                            if (!!$scope.insuredPersonData.DATEREPORTED) {
                                $scope.insuredPersonData.DATEREPORTED = formatDateToDatePicker($scope.insuredPersonData.DATEREPORTED);
                            }

                            if (!!$scope.insuredPersonData.CLAIMREGDATE) {
                                $scope.insuredPersonData.CLAIMREGDATE = formatDateToDatePicker($scope.insuredPersonData.CLAIMREGDATE);
                            }

                            if (!!$scope.insuredPersonData.DATEOFADMIT) {
                                $scope.insuredPersonData.DATEOFADMIT = formatDateToDatePicker($scope.insuredPersonData.DATEOFADMIT);
                            }

                            if (!!$scope.insuredPersonData.DATEOFBIRTH) {
                                $scope.insuredPersonData.DATEOFBIRTH = formatDateToDatePicker($scope.insuredPersonData.DATEOFBIRTH);
                            }
                        }
                        else {
                            if (!!$scope.ClaimInfo.FirstConsignmentData) {
                                $scope.insuredPersonData.DATEREPORTED = formatDateToDatePicker($scope.ClaimInfo.FirstConsignmentData);
                            }
                        }



                    } else if (TABNAME == "RESERVE") {
                        if (data.RESERVEDATA.items.length > 0)
                            $scope.reserveData = data.RESERVEDATA.items[0].indexedMap

                    }
                    else if (TABNAME == "COMMERCIALAPPROVAL") {
                        $scope.SPnameSearch ='';
                        $scope.Investigation = false;
                        $scope.Commercial = true;
                        $scope.INVESTIGATIONDETAILS={};
                        var d = new Date();
                        $scope.INVESTIGATIONDETAILS.APPOINTMENTDATE = formatDateToDatePicker(d);
                        
                        var expected = d;
                        expected.setDate(d.getDate()+7);
                        $scope.INVESTIGATIONDETAILS.EXPECTEDSUBMISSIONDATE = formatDateToDatePicker(expected);
                        $scope.CommercialCheckList = [];
                        if (data.data2.items.length > 0) {
                            for (var i = 0; i < data.data2.items.length; i++) {
                                $scope.CommercialCheckList[i] = {};
                                $scope.CommercialCheckList[i] = data.data2.items[i].indexedMap;
                                delete $scope.CommercialCheckList[i]["@metadata"];
                            }
                        }
                        else{
                        if (data.CommercialData.items.length > 0){
                            for (var i = 0; i < data.CommercialData.items.length;i++){
                            $scope.CommercialCheckList[i]={};
                            $scope.CommercialCheckList[i] = data.CommercialData.items[i].indexedMap;
                                delete $scope.CommercialCheckList[i]["@metadata"];
                            }
                        }
                        } 
                        if(!!data.data3.items){
                        if (data.data3.items.length > 0) {
                         
                                $scope.CASEDETAILS = {};
                                $scope.CASEDETAILS = data.data3.items[0].indexedMap;
                             
                            
                        }
                    }
                        $scope.CLAIMDETAILS = {};
                        if (!!data.data4.items) {
                            if (data.data4.items.length > 0) {
                                $scope.CLAIMDETAILS.PATIENTNAME = data.data4.items[0].indexedMap.FIRSTNAME;
                                $scope.CLAIMDETAILS.AGE = data.data4.items[0].indexedMap.AGE;
                                $scope.CLAIMDETAILS.GENDER = data.data4.items[0].indexedMap.GENDER;
                            }
                        }

                        if (!!data.data5.items) {
                            if (data.data5.items.length > 0) {


                                $scope.CLAIMDETAILS.PROVIDERNAME = data.data5.items[0].indexedMap.PROVIDERNAME;
                                $scope.CLAIMDETAILS.CITY = data.data5.items[0].indexedMap.CITY;
                                $scope.CLAIMDETAILS.TREATINGDOCTOR = data.data5.items[0].indexedMap.FIRSTNAME;


                            }
                        }

                        if (!!data.data6.items) {
                            if (data.data6.items.length > 0) {


                                $scope.CLAIMDETAILS.DATEOFADMISSIONLOSS = makeSQLServerComp(data.data6.items[0].indexedMap.DATEOFADMISSIONLOSS);
                                $scope.CLAIMDETAILS.DATEOFDISCHARGE = makeSQLServerComp(data.data6.items[0].indexedMap.DATEOFDISCHARGE);
                            }
                        }
                        if (!!data.data1.items) {
                            $scope.ICD10PCSDetails = [];
                            if (data.data1.items.length > 0) {
                                for (var i = 0; i < data.data1.items.length; i++){
                                    $scope.ICD10PCSDetails[i] = {};
                                    $scope.ICD10PCSDetails[i] = data.data1.items[i].indexedMap;
                               }
                            }
                        }

                        $scope.InvestigationDone = false;

                        if (!!data.INVESTIGATIONDATA){
                            if (data.INVESTIGATIONDATA.items.length > 0) {
                                $scope.InvestigationDone = true;

                            }
                        }

                        $scope.getTriggerPoints();
                }
                    else if (TABNAME == "ICDPCSCODING") {
                        $scope.searchICD = {};
                        $scope.deleteRowsICD = [];
                        $scope.ICD10PCSDETAILS = [];
                        $scope.ICDAdded = [];
                        $scope.ICDMode = '';

                        if (data.data2.items.length > 0) {
                            for (var i = 0; i < data.data2.items.length; i++) {
                                $scope.ICD10PCSDETAILS[i] = data.data2.items[i].indexedMap;
                            }
                        }

                        else {
                            $scope.ICD10PCSDETAILS[0] = {};
                            $scope.ICD10PCSDETAILS[1] = {};
                            $scope.ICDAdded[0] = true;
                            $scope.ICDAdded[1] = true;
                            $scope.ICD10PCSDETAILS[0].DIAGNOSIS = "Primary Diagnosis";
                            $scope.ICD10PCSDETAILS[1].DIAGNOSIS = "Additional Diagnosis";
                        }


                        if (data.data3.items.length > 0) {
                            $scope.DISEASECLASSIFICATION = data.data3.items[0].indexedMap;

                        }

                        else {
                            $scope.DISEASECLASSIFICATION = {};
                            $scope.getHospitalizationData();
                        }
                        if (!!data.data4 && data.data4.items.length > 0) {
                            $scope.CASEMAPPING = data.data4.items[0].indexedMap;
                        }
                        else {
                            $scope.CASEMAPPING = {};
                        }


                    } else if (TABNAME == "MEDICALAPPROVAL") {
                        $scope.medicalPastHistory = [];
                        $scope.medicalCheckList =[];
                        if (data.MEDICALPASTHISTORY.items.length > 0) {
                            for (var i = 0; i < data.MEDICALPASTHISTORY.items.length; i++) {
                                $scope.medicalPastHistory.push(data.MEDICALPASTHISTORY.items[i].indexedMap);
                                $scope.medicalPastHistory[i].DATESINCE = formatDateToDatePicker($scope.medicalPastHistory[i].DATESINCE);
                            }
                        }
                        if (data.MEDICALCHECKLIST.items.length > 0) {
                            for (var i = 0; i < data.MEDICALCHECKLIST.items.length; i++) {
                                $scope.medicalCheckList.push(data.MEDICALCHECKLIST.items[i].indexedMap);
                            }
                        }
                        if (data.CASEDETAILS.items.length > 0) {
                           
                        }
                        if (data.NONDISCLOSUREDATA.items.length > 0) {
                           $scope.NONDISCLOSURE = data.NONDISCLOSUREDATA.items[0].indexedMap;
                        }
                    } else {

                    }
                    $scope.$apply();
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in getting Claims Details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getTabData(serviceArgs);



        }
        $scope.OpenClaimScreen();

        $scope.openNav = function () {
            $('#leftNavBar').toggleClass('activeLeftTab');
        }
        function getPreAuthHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("PreAuthHtmlFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }

        function getInwardHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("InwardHtmlFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }


        function getCoderHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("CoderHtml.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }

        function getCommercialApprovalHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("CommercialApprovalHtml.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }

        function getMedicalApprovalHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("MedicalApprovalHtml.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }

        function getInwardOrPreauthCommonHtmlFileURL(filename) {
            return com_ibm_bpm_coach.getManagedAssetUrl("InwardAndPreuthHtmlFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
        }

        $scope.icdPcsCodingURL = getCoderHtmlFileURL('Coder');
        $scope.commercialApprovalURL = getCommercialApprovalHtmlFileURL('CommercialApproval');
        $scope.medicalPastHistoryURL = getMedicalApprovalHtmlFileURL('MedicalPastHistory');
        $scope.medicalAdjuducationURL = getMedicalApprovalHtmlFileURL('MedicalAdjudication');
        $scope.ReserveURL = getInwardOrPreauthCommonHtmlFileURL('reserve');

        if ($scope.TYPE == 'INWARD') {
            $scope.InsuredPersonHospitalizedDetailsURL = getInwardHtmlFileURL('InsuredPersonHospitalizedDetails');
            $scope.InsuranceHistoryURL = getInwardHtmlFileURL('InsuranceHistory');
            $scope.ProviderDetailsURL = getInwardOrPreauthCommonHtmlFileURL('ProviderDetails');
            $scope.HospitalizatinDetailsURL = getInwardHtmlFileURL('HospitalizatinDetails');
            $scope.BankDetailsURL = getInwardHtmlFileURL('BankDetails');
            $scope.InvoiceAdditionURL = getInwardOrPreauthCommonHtmlFileURL('InvoiceAddition');
            $scope.BillingScreenURL = getInwardOrPreauthCommonHtmlFileURL('BillingScreen');
            $scope.BenefitWiseScreenURL = getInwardHtmlFileURL('BenefitWiseScreen');
        }

        if ($scope.TYPE == 'PRE-AUTH') {
            $scope.MemberDetailsURL = getPreAuthHtmlFileURL('MemberDetails');
            $scope.ProviderDetailsURL = getInwardOrPreauthCommonHtmlFileURL('ProviderDetails');
            $scope.PatientDetailsFromHospitalURL = getPreAuthHtmlFileURL('PatientDetailsFromHospital');
            $scope.PatientAdmissionDetailsURL = getPreAuthHtmlFileURL('PatientAdmissionDetails');
            $scope.InvoiceAdditionURL = getInwardOrPreauthCommonHtmlFileURL('InvoiceAddition');
            $scope.BillingScreenURL = getInwardOrPreauthCommonHtmlFileURL('BillingScreen');

        }





        $scope.PrevOrNextTabSelection = function (clickedButton) {
            if (clickedButton == 'next') {
                $('.next-tab > .active').next('li').find('a').trigger('click');
            } else if (clickedButton == 'prev') {
                $('.next-tab > .active').prev('li').find('a').trigger('click');
            }
        }

        $scope.currentAdded = [];
        $scope.INSURANCEHISTORYDETAILS = {};

        $scope.CurrentInsuranceChanged = function () {
            if ($scope.INSURANCEHISTORYDETAILS.CURRENTINSURANCEFLAG == 'true') {
                $scope.CURRENTINSURANCE = [];
                $scope.CURRENTINSURANCE[0] = {};
                $scope.currentAdded[0] = true;
            }

            else {
                $scope.CURRENTINSURANCE = [];
                $scope.currentAdded = [];
            }
            //$scope.$apply();
        }

        $scope.HOSPITALIZATIONHISTORY = [];

        $scope.hospitalizationChanged = function () {
            if ($scope.INSURANCEHISTORYDETAILS.HOSPITALIZATION == 'true') {
                $scope.HOSPITALIZATIONHISTORY = [];
                $scope.hospitalizedAdded[0] = true;
                $scope.HOSPITALIZATIONHISTORY[0] = {};
            }
            else {
                $scope.hospitalizedAdded = [];
                $scope.HOSPITALIZATIONHISTORY = [];
            }
        }

        $scope.addRowCurrent = function () {
            $scope.deleteCurrent[$scope.CURRENTINSURANCE.length] = true;
            $scope.CURRENTINSURANCE[$scope.CURRENTINSURANCE.length] = {};
            $scope.currentAdded[$scope.CURRENTINSURANCE.length] = true;

        }


        $scope.hospitalizedAdded = [];

        $scope.addRowHospitalized = function () {
            $scope.deleteHospitalized[$scope.HOSPITALIZATIONHISTORY.length] = true;
            $scope.HOSPITALIZATIONHISTORY[$scope.HOSPITALIZATIONHISTORY.length] = {};
            $scope.hospitalizedAdded[$scope.HOSPITALIZATIONHISTORY.length] = true;
        }

        $scope.deleteRowCurrentInstance = function (index) {
            $scope.CURRENTINSURANCE.splice(index, 1);
            $scope.currentAdded[index] = false;
        }

        $scope.deleteRowHospitalized = function (index) {
            $scope.HOSPITALIZATIONHISTORY.splice(index, 1);
            $scope.hospitalizedAdded[index] = false;
        }

        $scope.getHospitalizationData = function () {
            var input = {

                "FEATURESUMMARYID": $scope.ClaimInfo.FEATURESUMMARYID,


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    $scope.injury = false;
                    if (!!data.HospitalizationDetails && data.HospitalizationDetails.items.length > 0) {
                        $scope.DISEASECLASSIFICATION.INJURYDETAILS = data.HospitalizationDetails.items[0].INJURYDETAILS;
                        $scope.DISEASECLASSIFICATION.CAUSE = data.HospitalizationDetails.items[0].CAUSE;
                        $scope.DISEASECLASSIFICATION.DISCHARGESTATUS = data.HospitalizationDetails.items[0].STATUSATDISCHARGE;
                        if (!!data.HospitalizationDetails.items[0].HOSPITALIZATIONDUETO && data.HospitalizationDetails.items[0].HOSPITALIZATIONDUETO == 'Injury') {
                            $scope.injury = true;
                        }
                        else {
                            $scope.injury = false;
                        }
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get hospitalization details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getHospitalizationData(serviceArgs);


        };

        $scope.getCaseManagement = function () {

            if (!!$scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID) {
                var caseID = $scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID;
            }
            else {
                var caseID = "";
            }

            var input = {

                "MemberID": 'MemberId-1',
                "CASEID": caseID


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if (!!data.CaseManagementDetails && data.CaseManagementDetails.items.length > 0) {
                        $scope.CaseManagement = data.CaseManagementDetails.items;
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get case details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getCaseManagement(serviceArgs);


        };

        $scope.addUpdateCommercialCheckList = function () {

            for (var i = 0; i < $scope.CommercialCheckList.length;i++){
            $scope.CommercialCheckList[i].FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            $scope.CommercialCheckList[i].INWARDID = $scope.ClaimInfo.INWARDID;
            delete $scope.CommercialCheckList[i]["$$hashKey"];
            }

            var input = {

                "CommercialChecklist": $scope.CommercialCheckList
            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    $scope.CommercialCheckList =[];
                    $scope.showSuccessMessage('Checklist Updated!');

                    if (!!data.output1 && data.CommercialCheckListOutput.items.length > 0) {
                        for (var i = 0; i < data.CommercialCheckListOutput.items.length; i++){
                            $scope.CommercialCheckList[i] = {};
                            $scope.CommercialCheckList[i] = data.CommercialCheckListOutput.items[i];
                        }
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get case details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addUpdateCommercialCheckList(serviceArgs);


        };

        $scope.addUpdateInvestigationRequest = function () {

            $scope.form.InvestigationDetails.submitted = true;
            if (!$scope.form.InvestigationDetails.$valid) {
                $scope.showWarningMessage('Enter mandatory data');
                return;
            }
            

            $scope.INVESTIGATIONDETAILS.EXPECTEDSUBMISSIONDATE = makeSQLServerComp($scope.INVESTIGATIONDETAILS.EXPECTEDSUBMISSIONDATE);
            $scope.INVESTIGATIONDETAILS.APPOINTMENTDATE = makeSQLServerComp($scope.INVESTIGATIONDETAILS.APPOINTMENTDATE);

            $scope.INVESTIGATIONDETAILS.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;

            var input = {

                "InvestigationRequest": $scope.INVESTIGATIONDETAILS,
                "ClaimNumber": $scope.ClaimInfo.CLAIMNUMBER
            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    $scope.showSuccessMessage('Investigation Request Sent!');
                    $scope.Commercial = true;
                    $scope.Investigation = false;
                    $scope.InvestigationDone = true;
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    
                    $scope.showErrorMessage(error, "Could not save details!");
                    $scope.Commercial = true;
                    $scope.Investigation = false;

                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addUpdateInvestigationRequest(serviceArgs);


        };

        $scope.redirectCommercial = function(page){
            if(page == 'investigator'){
            $scope.Commercial = false;
            $scope.Investigation = true;
            }
            else{
                $scope.Commercial = true;
                $scope.Investigation = false;
            }
           
        }

        $scope.getTriggerPoints = function () {

   
            var input = {

                "FEATURESUMMARYID": $scope.ClaimInfo.FEATURESUMMARYID
            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
            

                    if (!!data.TriggerPoints && data.TriggerPoints.items.length > 0) {
                        
                        $scope.triggerPoints = data.TriggerPoints.items[0].TRIGGERPOINTS.split(',');
                        $scope.triggerPoints.splice($scope.triggerPoints.length-1,1);
                       
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get trigger points!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getTriggerPoints(serviceArgs);


        };
        

        $scope.addCommercialApproval = function (status) {

            $scope.CommercialApproval = {};

            $scope.CommercialApproval.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            $scope.CommercialApproval.INWARDID = $scope.ClaimInfo.INWARDID;

            if (status == 'MEDICALADJUSTOR'){
                $scope.CommercialApproval.APPROVALSTATUS = 'Approved';
            }

            else if (status == 'DEO'){
                $scope.CommercialApproval.APPROVALSTATUS = 'Sent to DEO';
            }

            else if (status == 'Coder') {
                $scope.CommercialApproval.APPROVALSTATUS = 'Sent to Coder';
            }

          
            

            var input = {

                "CommercialApproval": $scope.CommercialApproval
            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                  
                    $scope.triggerFunctionCommercial(status);

               
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get case details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addCommercialApproval(serviceArgs);


        };


        $scope.triggerFunctionCommercial = function (redirect) {

            $scope.context.options.redirectTo.boundObject.redirectTo = redirect;
     

            this.context.trigger(function () {
                //console.log("jQuery button boundary event handled"); 
            })
        }

        $scope.ClearDiseaseClassification = function(){

            $scope.ICD10PCSDETAILS[0] = {};
            $scope.ICD10PCSDETAILS[1] = {};
            $scope.ICDAdded[0] = true;
            $scope.ICDAdded[1] = true;
            $scope.ICD10PCSDETAILS[0].DIAGNOSIS = "Primary Diagnosis";
            $scope.ICD10PCSDETAILS[1].DIAGNOSIS = "Additional Diagnosis";
            $scope.DISEASECLASSIFICATION = {};


        }

        $scope.checkListModal = function () {
            $('#CheckListModal').modal('toggle');
        }

        $scope.addUpdateCaseManagement = function (x) {

            var caseClaim = x;
            if (!!caseClaim.CASENAME) {
                $scope.CASEMAPPING.CASENAME = caseClaim.CASENAME;
            }
            if (!!caseClaim.CASETYPE) {
                $scope.CASEMAPPING.CASETYPE = caseClaim.CASETYPE;
            }
            if (!!caseClaim.SUBTYPE) {
                $scope.CASEMAPPING.CASESUBTYPE = caseClaim.SUBTYPE;
            }
            if (!!caseClaim.CASENAME) {
                $scope.CASEMAPPING.CASESTATUS = caseClaim.CASESTATUS;
            }

            $scope.CASEMAPPING.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;

            $scope.CASEMAPPING.INWARDID = $scope.ClaimInfo.INWARDID;
            $scope.CASEMAPPING.CLAIMNUMBER = $scope.ClaimInfo.CLAIMNUMBER;

            var input = {

                "caseClaimMapping": $scope.CASEMAPPING,


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if (!!data.caseClaimMappingDetails && data.caseClaimMappingDetails.items.length > 0) {
                        $scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID = data.caseClaimMappingDetails.items[0].HEALTHCASECLAIMMAPPINGID;
                    }
                    $scope.showSuccessMessage('Claim Successfully Tagged to Case');
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get case details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addUpdateCaseManagement(serviceArgs);


        };

        $scope.getICDData = function (pagStart) {


            $scope.ICDcurrentPosition = pagStart;


            var input = {
                "multipleSearchInput": [{ SearchParam: 'SHORTDESCRIPTION', searchText: $scope.searchICD.SHORTDESCRIPTION }, { SearchParam: 'LONGDESCRIPTION', searchText: $scope.searchICD.LONGDESCRIPTION }],
                "tableName": 'HEALTHICDCODESMASTER',
                "startPosition": (pagStart - 1) * 10,
                "orderBy": 'SHORTDESCRIPTION',
                "noOfRow": 5,
                "COLUMNS": '*',
                "ICDSearchMode": $scope.ICDMode
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (!!data.searchResults && data.searchResults.items.length > 0) {
                        $scope.ICDSearchData = data.searchResults.items;
                    }
                    else {
                        $scope.ICDSearchData = [];
                    }
                    if (!!data.RESULTCOUNT) {
                        $scope.ICDresultCount = data.RESULTCOUNT;
                    }
                    $scope.ICDassignPagVariables();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMultipleSearchICD(serviceArgs);
        }

        $scope.getServiceProvider = function (pagStart) {

            if ($scope.INVESTIGATIONDETAILS.ASSIGNTO == 'Internal Investigation Team') {
                var typeOfSP = 'Internal';
            }

            else {
                var typeOfSP = 'External';
            }



            var input = {
                "multipleSearchInput": [{ SearchParam: 'TYPE', searchText: typeOfSP }, { SearchParam: 'NAME', searchText: $scope.SPnameSearch}],
                "tableName": 'RESOURCEMASTER',
                "startPosition": (pagStart - 1) * 10,
                "orderBy": 'NAME',
                "noOfRow": 5,
                "COLUMNS": '*',

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (!!data.searchResults && data.searchResults.items.length > 0) {
                        $scope.ServiceProviderList = data.searchResults.items;
                    }
                    else {
                        $scope.ServiceProviderList = [];
                    }
                    if (!!data.RESULTCOUNT) {
                        $scope.SPresultCount = data.RESULTCOUNT;
                    }
                    $scope.assignPagVariables();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMultipleSearch(serviceArgs);
        }

        $scope.SPcurrentPosition = 1;
        $scope.SPresultCount = 0;
        $scope.SPtotalPages = 0;
        $scope.SPpageSize = 2;

        $scope.assignPagVariables = function () {

            $scope.SPtotalPages = Math.ceil($scope.SPresultCount / $scope.pageSize);

            switch ($scope.SPtotalPages) {
                case 1:
                    $scope.SPpreviousPage = 0;
                    $scope.SPnextPage = 0;
                    $scope.SPmiddlePage = 1;
                    break;
                case 2:
                    if ($scope.SPcurrentPosition == 1) {
                        $scope.SPnextPage = 2;
                        $scope.SPpreviousPage = 0;

                    } else if ($scope.SPcurrentPosition == 2) {
                        $scope.SPpreviousPage = 1;
                        $scope.SPnextPage = 0;
                    }
                    $scope.SPmiddlePage = $scope.SPcurrentPosition;
                    break;
                default:
                    if ($scope.SPcurrentPosition == 1) {

                        $scope.SPpreviousPage = 1;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition + 1;
                        $scope.SPnextPage = $scope.SPcurrentPosition + 2;

                    } else if ($scope.SPcurrentPosition == $scope.SPtotalPages) {

                        $scope.SPpreviousPage = $scope.SPcurrentPosition - 2;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition - 1;
                        $scope.SPnextPage = $scope.SPcurrentPosition;

                    } else {
                        $scope.SPpreviousPage = $scope.SPcurrentPosition - 1;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition;
                        $scope.SPnextPage = $scope.SPcurrentPosition + 1;
                    }
            }

        };

        $scope.selectedSP = function(x){
            $scope.INVESTIGATIONDETAILS.INVESTIGATORNAME = x.NAME;
            $scope.INVESTIGATIONDETAILS.INVESTIGATORNTID = x.USERID;
          
            $('#SPSearchModal').modal('toggle');
        }

        $scope.SPModalToggle = function(){
            if (!!$scope.INVESTIGATIONDETAILS.ASSIGNTO) {
                $('#SPSearchModal').modal('toggle');
            }
            else{
                $scope.showWarningMessage('Select type of Service Provider!');
            }
        }

        $scope.ICDModalToggle = function (x) {
            $scope.ICDcurrentPosition = 1;
            $scope.ICDresultCount = 0;
            $scope.ICDtotalPages = 0;
            $scope.ICDpageSize = 5;
            $scope.searchICD = {};
            $scope.ICDSearchData = [];
            $scope.ICDMode = x;
            $('#ICDCodeModal').modal('toggle');
        }


        $scope.ICDModalToggleICD = function (x) {
            $scope.ICDcurrentPosition = 1;
            $scope.ICDresultCount = 0;
            $scope.ICDtotalPages = 0;
            $scope.ICDpageSize = 5;
            $scope.searchICD = {};
            $scope.ICDSearchData = [];
            $scope.ICDMode = 'ICD';
            $scope.ICDIndex = x;
            $('#ICDCodeModal').modal('toggle');
        }

        $scope.ICDcurrentPosition = 1;
        $scope.ICDresultCount = 0;
        $scope.ICDtotalPages = 0;
        $scope.ICDpageSize = 5;

        $scope.ICDassignPagVariables = function () {

            $scope.ICDtotalPages = Math.ceil($scope.ICDresultCount / $scope.ICDpageSize);

            switch ($scope.ICDtotalPages) {
                case 1:
                    $scope.ICDpreviousPage = 0;
                    $scope.ICDnextPage = 0;
                    $scope.ICDmiddlePage = 1;
                    break;
                case 2:
                    if ($scope.ICDcurrentPosition == 1) {
                        $scope.ICDnextPage = 2;
                        $scope.ICDpreviousPage = 0;

                    } else if ($scope.ICDcurrentPosition == 2) {
                        $scope.ICDpreviousPage = 1;
                        $scope.ICDnextPage = 0;
                    }
                    $scope.ICDmiddlePage = $scope.ICDcurrentPosition;
                    break;
                default:
                    if ($scope.ICDcurrentPosition == 1) {

                        $scope.ICDpreviousPage = 1;
                        $scope.ICDmiddlePage = $scope.ICDcurrentPosition + 1;
                        $scope.ICDnextPage = $scope.ICDcurrentPosition + 2;

                    } else if ($scope.ICDcurrentPosition == $scope.ICDtotalPages) {

                        $scope.ICDpreviousPage = $scope.ICDcurrentPosition - 2;
                        $scope.ICDmiddlePage = $scope.ICDcurrentPosition - 1;
                        $scope.ICDnextPage = $scope.ICDcurrentPosition;

                    } else {
                        $scope.ICDpreviousPage = $scope.ICDcurrentPosition - 1;
                        $scope.ICDmiddlePage = $scope.ICDcurrentPosition;
                        $scope.ICDnextPage = $scope.ICDcurrentPosition + 1;
                    }
            }

        };


        $scope.selectICDCode = function (x) {
            if ($scope.ICDMode == 'Injury') {
                $scope.DISEASECLASSIFICATION.ICDCODE = x.ICDCODE;
                $scope.DISEASECLASSIFICATION.SHORTDESCRIPTION = x.SHORTDESCRIPTION;
                $scope.DISEASECLASSIFICATION.LONGDESCRIPTION = x.LONGDESCRIPTION;
            }
            else if ($scope.ICDMode == 'ICD') {
                $scope.ICD10PCSDETAILS[$scope.ICDIndex].ICDCODE = x.ICDCODE;
                $scope.ICD10PCSDETAILS[$scope.ICDIndex].SHORTDESCRIPTION = x.SHORTDESCRIPTION;
                $scope.ICD10PCSDETAILS[$scope.ICDIndex].LONGDESCRIPTION = x.LONGDESCRIPTION;

            }
            else {
                $scope.DISEASECLASSIFICATION.DISCHARGEICDCODE = x.ICDCODE;
                $scope.DISEASECLASSIFICATION.DISCHARGESHORTDESCRIPTION = x.SHORTDESCRIPTION;
                $scope.DISEASECLASSIFICATION.DISCHARGELONGDESCRIPTION = x.LONGDESCRIPTION;
            }
            $('#ICDCodeModal').modal('toggle');

        }

        $scope.deleteRowICD10 = function (index) {
            if ($scope.ICD10PCSDETAILS.length == 1) {
                $scope.showWarningMessage('ICD 10 PCS Details are mandatory');
            }
            else {
                if (!!$scope.ICD10PCSDETAILS[index].HEALTHICD10PCSDETAILSID) {
                    $scope.deleteRowsICD[$scope.deleteRowsICD.length] = '';
                    $scope.deleteRowsICD[$scope.deleteRowsICD.length] = $scope.ICD10PCSDETAILS[index].HEALTHICD10PCSDETAILSID;
                }
                $scope.ICD10PCSDETAILS.splice(index, 1);
            }

        }

        $scope.addRowICDAdded = function () {
            var i = $scope.ICD10PCSDETAILS.length;
            $scope.ICDAdded[$scope.ICD10PCSDETAILS.length] = true;

            if ($scope.ICD10PCSDETAILS.length >= 2) {
                $scope.ICD10PCSDETAILS[$scope.ICD10PCSDETAILS.length] = {};
                $scope.ICD10PCSDETAILS[i].DIAGNOSIS = "Co-morbidities";
            }
            else {

                if ($scope.ICD10PCSDETAILS.length == 0) {
                    $scope.ICD10PCSDETAILS[$scope.ICD10PCSDETAILS.length] = {};
                    $scope.ICD10PCSDETAILS[0].DIAGNOSIS = "Primary Diagnosis";
                }
                else if ($scope.ICD10PCSDETAILS.length == 1) {
                    $scope.ICD10PCSDETAILS[$scope.ICD10PCSDETAILS.length] = {};
                    $scope.ICD10PCSDETAILS[1].DIAGNOSIS = "Additional Diagnosis";
                }

            }
        }


        $scope.addUpdateDiseaseClassification = function () {

            $scope.form.diseaseClassification.submitted = true;
            if (!$scope.form.diseaseClassification.$valid) {
                $scope.showWarningMessage('Enter mandatory data');
                return;
            }

            if ($scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID == undefined || $scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID == null || $scope.CASEMAPPING.HEALTHCASECLAIMMAPPINGID == "") {

                $scope.showWarningMessage('Tag Case to Claim to proceed');
                return;
            }

            for (var i = 0; i < $scope.ICD10PCSDETAILS.length; i++) {
                delete $scope.ICD10PCSDETAILS[i]["$$hashKey"];
                $scope.ICD10PCSDETAILS[i].INWARDID = $scope.ClaimInfo.INWARDID;
                $scope.ICD10PCSDETAILS[i].FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            }

            $scope.DISEASECLASSIFICATION.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            $scope.DISEASECLASSIFICATION.INWARDID = $scope.ClaimInfo.INWARDID;

            var input = {

                "diseaseClassification": $scope.DISEASECLASSIFICATION,
                "ICD10PCSDetails": $scope.ICD10PCSDETAILS,
                "deleteRowsICD": $scope.deleteRowsICD
            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if (!!data.HealthDiseaseClassification && data.HealthDiseaseClassification.items.length > 0) {
                        $scope.DISEASECLASSIFICATION.HEALTHDISEASECLASSIFICATIONID = data.HealthDiseaseClassification.items[0].HEALTHDISEASECLASSIFICATIONID;
                    }

                    if (!!data.ICD10PCSDETAILSLIST && data.ICD10PCSDETAILSLIST.items.length > 0) {
                        $scope.ICD10PCSDETAILS = data.ICD10PCSDETAILSLIST.items;
                    }

                    $scope.showSuccessMessage('Data Inserted/Updated!');

                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not save details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addUpdateDiseaseClassification(serviceArgs);


        };


        $scope.AddUpdateInsuredDetails = function (clickedButton) {

            $scope.form.detailsEntryForm.submitted = true;
            if (!$scope.form.detailsEntryForm.$valid) {
                $scope.showWarningMessage('Enter mandatory data');
                return;
            }

            if (!!$scope.insuredPersonData.DATEREPORTED) {
                $scope.insuredPersonData.DATEREPORTED = makeSQLServerComp($scope.insuredPersonData.DATEREPORTED);
            }

            if (!!$scope.insuredPersonData.CLAIMREGDATE) {
                $scope.insuredPersonData.CLAIMREGDATE = makeSQLServerComp($scope.insuredPersonData.CLAIMREGDATE);
            }

            if (!!$scope.insuredPersonData.DATEOFADMIT) {
                $scope.insuredPersonData.DATEOFADMIT = makeSQLServerComp($scope.insuredPersonData.DATEOFADMIT);
            }

            if (!!$scope.insuredPersonData.DATEOFBIRTH) {
                $scope.insuredPersonData.DATEOFBIRTH = makeSQLServerComp($scope.insuredPersonData.DATEOFBIRTH);
            }

            $scope.insuredPersonData.INWARDID = $scope.ClaimInfo.INWARDID;
            $scope.insuredPersonData.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;

            $scope.insuredPersonData.CLAIMEDAMT = parseInt($scope.insuredPersonData.CLAIMEDAMT);



            var input = {

                "InsuredPolicyDetails": $scope.insuredPersonData,


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if (!!data.InsuredPolicyDetailsOutput && data.InsuredPolicyDetailsOutput.items.length > 0) {
                        $scope.insuredPersonData.HEALTHPOLICYINSUREDPERSONDETAILSID = data.InsuredPolicyDetailsOutput.items[0].HEALTHPOLICYINSUREDPERSONDETAILSID;
                    }
                    if (!!$scope.insuredPersonData.DATEREPORTED) {
                        $scope.insuredPersonData.DATEREPORTED = formatDateToDatePicker($scope.insuredPersonData.DATEREPORTED);
                    }

                    if (!!$scope.insuredPersonData.CLAIMREGDATE) {
                        $scope.insuredPersonData.CLAIMREGDATE = formatDateToDatePicker($scope.insuredPersonData.CLAIMREGDATE);
                    }

                    if (!!$scope.insuredPersonData.DATEOFADMIT) {
                        $scope.insuredPersonData.DATEOFADMIT = formatDateToDatePicker($scope.insuredPersonData.DATEOFADMIT);
                    }

                    if (!!$scope.insuredPersonData.DATEOFBIRTH) {
                        $scope.insuredPersonData.DATEOFBIRTH = formatDateToDatePicker($scope.insuredPersonData.DATEOFBIRTH);
                    }
                    if (clickedButton != 'save') {
                        $scope.PrevOrNextTabSelection(clickedButton);
                    }

                    $scope.showSuccessMessage('Data Sucessfully inserted/updated');
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not save!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.AddUpdateInsuredDetails(serviceArgs);


        };

        $scope.addUpdateInsuranceHistoryDetails = function (clickedButton) {

            $scope.form.InsuranceHistory.submitted = true;
            if (!$scope.form.InsuranceHistory.$valid) {
                $scope.showWarningMessage('Enter mandatory data');
                return;
            }


            if (!!$scope.CURRENTINSURANCE) {
                for (var i = 0; i < $scope.CURRENTINSURANCE.length; i++) {
                    delete $scope.CURRENTINSURANCE[i]["$$hashKey"];
                    $scope.CURRENTINSURANCE[i].INWARDID = $scope.ClaimInfo.INWARDID;
                    $scope.CURRENTINSURANCE[i].FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
                }
            }
            if (!!$scope.HOSPITALIZATIONHISTORY) {
                for (var i = 0; i < $scope.HOSPITALIZATIONHISTORY.length; i++) {
                    delete $scope.HOSPITALIZATIONHISTORY[i]["$$hashKey"];
                    $scope.HOSPITALIZATIONHISTORY[i].INWARDID = $scope.ClaimInfo.INWARDID;
                    $scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE = makeSQLServerComp($scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE);
                    $scope.HOSPITALIZATIONHISTORY[i].FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
                }
            }

            if (!!$scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT) {
                $scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT = makeSQLServerComp($scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT);
            }

            $scope.INSURANCEHISTORYDETAILS.INWARDID = $scope.ClaimInfo.INWARDID;
            $scope.INSURANCEHISTORYDETAILS.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;


            var input = {

                "InsuranceHistoryDetails": $scope.INSURANCEHISTORYDETAILS,
                "CurrentInsurance": $scope.CURRENTINSURANCE,
                "HospitalizationHistory": $scope.HOSPITALIZATIONHISTORY,


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if (!!data.InsuranceHistoryDetailsOutput && data.InsuranceHistoryDetailsOutput.items.length > 0) {
                        $scope.INSURANCEHISTORYDETAILS.HEALTHINSURANCEHISTORYDETAILSID = data.InsuranceHistoryDetailsOutput.items[0].HEALTHINSURANCEHISTORYDETAILSID;
                    }

                    if (!!data.CurrentInsuranceOutput) {
                        if (!!data.CurrentInsuranceOutput.items && data.CurrentInsuranceOutput.items.length > 0) {

                            $scope.CURRENTINSURANCE = data.CurrentInsuranceOutput.items;


                        }
                    }


                    if (!!data.HospitalizationHistoryOutput) {
                        if (!!data.HospitalizationHistoryOutput.items && data.HospitalizationHistoryOutput.items.length > 0) {

                            $scope.HOSPITALIZATIONHISTORY = data.HospitalizationHistoryOutput.items;


                        }
                    }

                    if (!!$scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT) {
                        $scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT = formatDateToDatePicker($scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT);
                    }

                    for (var i = 0; i < $scope.HOSPITALIZATIONHISTORY.length; i++) {

                        $scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE = formatDateToDatePicker($scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE);

                    }

                    $scope.deleteHospitalized = [];
                    $scope.deleteCurrent = [];

                    if (clickedButton != 'save') {
                        $scope.PrevOrNextTabSelection(clickedButton);
                    }

                    $scope.showSuccessMessage('Data Sucessfully inserted/updated');

                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not insert!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.AddUpdateInsuranceHistoryDetails(serviceArgs);
        };


        $scope.getClaimNumberForFeatureClaim = function () {

            var input = {
                "searchText": $scope.featureClaimSearch.searchText,
                "SearchParam": $scope.featureClaimSearch.SearchParam,
                "tableName": "HEALTHFEATURESUMMARY",
                "startPosition": $scope.currentPosition - 1,
                "orderBy": "CLAIMNUMBER DESC",
                "noOfRow": 7
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    if (data.searchResults.items.length > 0) {
                        $scope.memberClaimData = data.searchResults.items;
                    } else {
                        $scope.memberClaimData = {};
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while getting claimnumber for featureclaim");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getSearchData(serviceArgs);

        }

        $scope.AutoPopulateClaimRegistrationData = function (FEATURESUMMARYID) {
            debugger;
            var input = {
                "TABNAME": 'CLAIMREGISTRATIONTAB',
                "INWARDID": $scope.ClaimInfo.INWARDID,
                "FEATURESUMMARYID": FEATURESUMMARYID

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.data1.items.length > 0) {
                        $scope.ProviderDetails = data.data1.items[0].indexedMap;
                        $scope.ProviderDetails.REGISTRATIONVALIDUPTO = makePickerComp($scope.ProviderDetails.REGISTRATIONVALIDUPTO);
                        $scope.ProviderDetails.HEALTHPROVIDERDETAILSID = null;
                        $scope.ProviderDetails.FEATURESUMMARYID = null;
                    }
                    if (data.data5.items.length > 0) {
                        $scope.BankDetails = data.data5.items[0].indexedMap;
                        $scope.BankDetails.HEALTHBANKDETAILSID = null;
                        $scope.BankDetails.FEATURESUMMARYID = null;
                    }
                    if (data.data2.items.length > 0) {
                        $scope.INSURANCEHISTORYDETAILS = data.data2.items[0].indexedMap;
                        $scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT = formatDateToDatePicker($scope.INSURANCEHISTORYDETAILS.DATEOFCOMMENCEMENT);
                        $scope.INSURANCEHISTORYDETAILS.HEALTHINSURANCEHISTORYDETAILSID = null;
                    }
                    if (data.data3.items.length > 0)
                        $scope.CURRENTINSURANCE = [];
                    $scope.currentAdded = [];
                    $scope.deleteCurrent = [];
                    $scope.deleteHospitalized = [];
                    for (var i = 0; i < data.data3.items.length; i++) {
                        $scope.CURRENTINSURANCE[i] = data.data3.items[i].indexedMap;
                        $scope.currentAdded[i] = true;
                    }

                    if (data.data4.items.length > 0)
                        $scope.HOSPITALIZATIONHISTORY = [];
                    $scope.hospitalizedAdded = [];
                    for (var i = 0; i < data.data4.items.length; i++) {
                        $scope.HOSPITALIZATIONHISTORY[i] = data.data4.items[i].indexedMap;
                        $scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE = formatDateToDatePicker($scope.HOSPITALIZATIONHISTORY[i].HOSPITALIZATIONDATE);
                        $scope.hospitalizedAdded[i] = true;
                    }

                    if (data.data6.items.length > 0) {
                        $scope.insuredPersonData = data.data6.items[0].indexedMap;
                        if (!!$scope.insuredPersonData.DATEREPORTED) {
                            $scope.insuredPersonData.DATEREPORTED = formatDateToDatePicker($scope.insuredPersonData.DATEREPORTED);
                        }

                        if (!!$scope.insuredPersonData.CLAIMREGDATE) {
                            $scope.insuredPersonData.CLAIMREGDATE = formatDateToDatePicker($scope.insuredPersonData.CLAIMREGDATE);
                        }

                        if (!!$scope.insuredPersonData.DATEOFADMIT) {
                            $scope.insuredPersonData.DATEOFADMIT = formatDateToDatePicker($scope.insuredPersonData.DATEOFADMIT);
                        }

                        if (!!$scope.insuredPersonData.DATEOFBIRTH) {
                            $scope.insuredPersonData.DATEOFBIRTH = formatDateToDatePicker($scope.insuredPersonData.DATEOFBIRTH);
                        }
                        $scope.insuredPersonData.FEATURENUMBER = null;
                        $scope.insuredPersonData.FEATURESUMMARYID = null;
                        $scope.insuredPersonData.BENEFITCLAIMED = null;
                        $scope.insuredPersonData.HEALTHPOLICYINSUREDPERSONDETAILSID = null;
                        $scope.insuredPersonData.CLAIMCATEGORY = 'FEATURE';
                        $scope.insuredPersonData.CLAIMEDAMT = '';
                    }
                    else {
                        if (!!$scope.ClaimInfo.FirstConsignmentData) {
                            $scope.insuredPersonData.DATEREPORTED = formatDateToDatePicker($scope.ClaimInfo.FirstConsignmentData);
                        }
                    }
                    $('#featureClaimModal').modal('hide');
                    $scope.$apply();
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in getting Claims Details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getTabData(serviceArgs);
        }



        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.IFSCalertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        };

        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = true;
            $scope.warning = false;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        };

        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;
            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        };

        $scope.showWarningMessage = function (warningmsg) {
            if ($scope.IFSCalertModal == false || $scope.IFSCalertModal == undefined)
                $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = false;
            $scope.warning = true;
            $scope.customeError = false;
            $scope.errCode = "Warning!!";
            $scope.errDesc = warningmsg;

            // $scope.$apply();

            if ($scope.errDesc == "Upload Files Contains Harmful Files !!")
                $scope.$apply();

            alertTimeoutCallback(5000);

        };

        //date picker and conversion functions

        var makeSQLServerComp = function (date) {
            if (!date)
                return null;
            else {
                var pos = date.indexOf("-");
                if (pos == 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var makePickerComp = function (date) {
            if (!date) {
                return null;
            } else {
                var pos = date.indexOf("-");
                if (pos != 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var formatDateToDatePicker = function (date) {

            if (!date) {
                return null;
            }
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
            // return [year, day, month].join('-');
        };


        //INSURED PERSON DETAILS SCOPE VARIABLES
        $scope.searchMember = {};
        $scope.insuredPersonData = {
            'AIGEMPLOYEE': 'No',
            'CATASTROPHE': 'No',
            'ISALTADDRESS': 'No',
            'ISALTMOBILE': 'No',
            'ORPHANCLAIM': 'No',
            'SERVICECLAIM': 'No',
            'CLAIMCATEGORY': 'NEW'
        };

        //insured person details screen
        $scope.validateMemberDetailsEntry = function () {
            if ($scope.searchMember.SEARCHMEMBER == '' || $scope.searchMember.SEARCHMEMBER == undefined) {
                $scope.showWarningMessage('Search Text Missing');
                return;
            }
            $scope.SEARCHATTR = ($scope.searchMember.SEARCHATTR == 'Policy Number') ? 'POLICYNUMBER' : 'MEMBERID';
            $scope.getMemberDetails(1);
        }
        $scope.getMemberDetails = function (pageStart) {

            $scope.currentPosition = pageStart;
            var input = {
                "searchText": $scope.searchMember.SEARCHMEMBER,
                "SearchParam": $scope.SEARCHATTR,
                "tableName": "HEALTHPOLICYDETAILSGC",
                "startPosition": $scope.currentPosition - 1,
                "orderBy": $scope.SEARCHATTR,
                "noOfRow": $scope.pageSize
            };
            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {

                    if (data.searchResults.items.length > 0) {
                        $scope.tableData = false;

                        $scope.memberDataList = data.searchResults.items;
                        $scope.resultCount = data.RESULTCOUNT;
                        $scope.assignPagVariables();

                    }
                    else {
                        $scope.memberDataList = [];
                        $scope.tableData = true;
                        $scope.totalPages = 0;
                    }
                    $scope.searchMember.SEARCHMEMBER = '';
                    $scope.$apply();
                    //$scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.searchMember.SEARCHMEMBER = '';
                    $scope.$apply();
                    //$scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Getting Member Data");
                }
            };

            //$scope.incServiceCallCounter();
            $scope.context.options.getMemberDetailsService(serviceArgs);
        };

        $scope.currentPosition = 1;
        $scope.resultCount = 0;
        $scope.totalPages = 0;
        $scope.pageSize = 2;

        $scope.assignPagVariables = function () {

            $scope.totalPages = Math.ceil($scope.resultCount / $scope.pageSize);

            switch ($scope.totalPages) {
                case 1:
                    $scope.previousPage = 0;
                    $scope.nextPage = 0;
                    $scope.middlePage = 1;
                    break;
                case 2:
                    if ($scope.currentPosition == 1) {
                        $scope.nextPage = 2;
                        $scope.previousPage = 0;

                    } else if ($scope.currentPosition == 2) {
                        $scope.previousPage = 1;
                        $scope.nextPage = 0;
                    }
                    $scope.middlePage = $scope.currentPosition;
                    break;
                default:
                    if ($scope.currentPosition == 1) {

                        $scope.previousPage = 1;
                        $scope.middlePage = $scope.currentPosition + 1;
                        $scope.nextPage = $scope.currentPosition + 2;

                    } else if ($scope.currentPosition == $scope.totalPages) {

                        $scope.previousPage = $scope.currentPosition - 2;
                        $scope.middlePage = $scope.currentPosition - 1;
                        $scope.nextPage = $scope.currentPosition;

                    } else {
                        $scope.previousPage = $scope.currentPosition - 1;
                        $scope.middlePage = $scope.currentPosition;
                        $scope.nextPage = $scope.currentPosition + 1;
                    }
            }

        };

        $scope.populateMemberData = function (member) {
            $scope.insuredPersonData.POLICYNUMBER = member.POLICYNUMBER;
            $scope.insuredPersonData.MEMBERID = member.MEMBERID;
            $scope.insuredPersonData.FIRSTNAME = member.FIRSTNAME;
            $scope.insuredPersonData.LASTNAME = member.LASTNAME;
            $scope.insuredPersonData.MOBILENUMBER = member.MOBILENUMBER;
            $scope.insuredPersonData.EMAILID = member.EMAILID;
            $scope.insuredPersonData.PRODUCTNAME = member.PRODUCTNAME;
            $scope.insuredPersonData.CERTIFICATENUMBER = member.CERTIFICATENUMBER;
            $scope.insuredPersonData.CORPORATENAME = member.CORPORATENAME;
            $scope.insuredPersonData.EMPLOYEEID = member.EMPLOYEEID;
            $scope.insuredPersonData.MIDDLENAME = member.MIDDLENAME;
            $scope.insuredPersonData.DATEOFBIRTH = member.DATEOFBIRTH;
            $scope.insuredPersonData.AGE = member.AGE;
            $scope.insuredPersonData.GENDER = member.GENDER;
            $scope.insuredPersonData.RELATIONSHIPTOPRIMARYINSURED = member.RELATIONSHIPTOPRIMARYINSURED;
            $scope.insuredPersonData.ADDRESS_LINE1 = member.ADDRESS;
            $scope.insuredPersonData.ADDRESS_LINE2 = member.ADDRESSLINE2;
            $scope.insuredPersonData.ADDRESS_LINE3 = member.ADDRESSLINE3;
            $scope.insuredPersonData.LANDMARK = member.LANDMARK;
            $scope.insuredPersonData.AREA = member.AREA;
            $scope.insuredPersonData.CITY = member.CITY;
            $scope.insuredPersonData.DISTRICT = member.DISTRICT;
            $scope.insuredPersonData.PINCODE = member.PINCODE;
            $scope.insuredPersonData.STATE = member.STATE;


            $scope.insuredPersonData.DATEOFBIRTH = formatDateToDatePicker(member.DATEOFBIRTH);
            $scope.insuredPersonData.AIGEMPLOYEE = 'No';
            $scope.insuredPersonData.CATASTROPHE = 'No';
            $scope.insuredPersonData.ISALTADDRESS = 'No';
            $scope.insuredPersonData.ISALTMOBILE = 'No';
            $scope.insuredPersonData.ORPHANCLAIM = 'No';
            $scope.insuredPersonData.SERVICECLAIM = 'No';
            $scope.clearMemberSearchData();
            $('#memberModal').modal('hide');
        };

        $scope.clearMemberSearchData = function () {
            $scope.memberDataList = [];
            $scope.resultCount = 0;
            $scope.tableData = false;
            $scope.totalPages = 0;
            $scope.searchMember.SEARCHMEMBER = '';
        };

        $scope.resetMemberData = function () {

            $scope.insuredPersonData = {};
            $scope.insuredPersonData.AIGEMPLOYEE = 'No';
            $scope.insuredPersonData.CATASTROPHE = 'No';
            $scope.insuredPersonData.ISALTADDRESS = 'No';
            $scope.insuredPersonData.ISALTMOBILE = 'No';
            $scope.insuredPersonData.ORPHANCLAIM = 'No';
            $scope.insuredPersonData.SERVICECLAIM = 'No';
            $scope.insuredPersonData.CLAIMCATEGORY = 'NEW';
            $scope.form.detailsEntryForm.$submitted = false;
            $scope.form.detailsEntryForm.$setUntouched();

        };

        $scope.validateMemberData = function () {

            $scope.form.detailsEntryForm.$submitted = true;
            if ($scope.form.detailsEntryForm.$invalid) {
                $scope.showWarningMessage('Invalid Form Entry');
                return;
            }
            $scope.saveMemberData();
        };

        $scope.saveMemberData = function () {
            debugger;

        };

        //billing screen
        $scope.submitBilling = function () {
            var input = {
                "INWARDID": $scope.ClaimInfo.INWARDID,
                "FEATURESUMMARYID": $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    var regData = data.RESULT.items;
                    if (regData[0].CLAIMGENERATION == 'ERROR') {
                        $scope.showWarningMessage(regData[1].WARNING);
                    } else if (regData[0].CLAIMGENERATION == 'SUCCESS') {
                        console.log(regData[1].CLAIMNUMBER);
                        $scope.CLAIMNUMBER = regData[1].CLAIMNUMBER;
                        $('#ClaimNumberModal').modal('show');

                        // $scope.endUserTask();  
                    } else if (regData[0].CLAIMGENERATION == 'CLAIMEXIST') {
                        $scope.showWarningMessage(regData[1].WARNING +' ClaimNumber '+ $scope.claimSummary.CLAIMNUMBER);

                        $timeout(function () {
                            $scope.context.trigger();
                        }, 2000)
                    } else {
                        $scope.showErrorMessage('something went wrong');
                    }
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in claim registration");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.submitClaimRegistration(serviceArgs);


        };


        //provider Details


        $scope.openProviderSearchModel = function (type) {
            debugger
            if (type == 'hospital') {
                $scope.prnModelHeader = 'Hospoital Details';
                $scope.REGISTEREDENTITYTYPE = 'INSTITUTION';
            } else if (type == 'doctor') {
                $scope.prnModelHeader = 'Treating Doctor';
                $scope.REGISTEREDENTITYTYPE = 'INDIVIDUAL';
            }

            $('#PRNSearchModal').modal('show');
        }


        $scope.getProviderData = function () {

            var input = {
                "multipleSearchInput": [{ SearchParam: 'PRN', searchText: $scope.providerSearch.PRN }, { SearchParam: 'FIRSTNAME', searchText: $scope.providerSearch.FIRSTNAME }, { SearchParam: 'PAN', searchText: $scope.providerSearch.PAN },
                { SearchParam: 'MOBILENUMBER', searchText: $scope.providerSearch.MOBILENUMBER },
                { SearchParam: 'REGISTRATIONNUMBER', searchText: $scope.providerSearch.REGISTRATIONNUMBER }, { SearchParam: 'CITY', searchText: $scope.providerSearch.CITY }, { SearchParam: 'REGISTEREDENTITY', searchText: $scope.REGISTEREDENTITYTYPE }],
                "tableName": 'HEALTHPROVIDERMANAGEMENTMASTER',
                "startPosition": 0,
                "orderBy": 'HEALTHPROVIDERMANAGEMENTMASTERID',
                "noOfRow": 7,
                "COLUMNS": '*'
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (data.searchResults.items.length > 0) {
                        $scope.providerSearchData = data.searchResults.items;
                    }
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMultipleSearch(serviceArgs);
        }

        $scope.selectedProvider = function (data) {
            if ($scope.REGISTEREDENTITYTYPE == 'INSTITUTION') {
                $scope.ProviderDetails.PROVIDERNAME = data.ENTITYNAME;
                $scope.ProviderDetails.PROVIDERALIASNAME = data.ALIASNAME;
                $scope.ProviderDetails.HOSPITALPRN = data.PRN;
                $scope.ProviderDetails.ADDRESSLINE1 = data.ADDRESSLINE1;
                $scope.ProviderDetails.ADDRESSLINE2 = data.ADDRESSLINE2;
                $scope.ProviderDetails.ADDRESSLINE3 = data.ADDRESSLINE3;
                $scope.ProviderDetails.PINCODE = data.PINCODE;
                $scope.ProviderDetails.DISTRICT = data.DISTRICT;
                $scope.ProviderDetails.CITY = data.CITY;
                $scope.ProviderDetails.STATE = data.STATE;
                $scope.ProviderDetails.COUNTRY = data.COUNTRY;
                $scope.ProviderDetails.PROVIDERTYPE = data.PROVIDERTYPE;
                $scope.ProviderDetails.ROHINICODE = data.ROHINIID;
                $scope.ProviderDetails.PROVIDERSTATUS = data.PROVIDERSTATUS;
                $scope.ProviderDetails.PROVIDERFLAG = data.PROVIDERFLAG;
                $scope.ProviderDetails.NOOFBED = data.NUMBEROFBEDS;
                $scope.ProviderDetails.REGISTRATIONVALIDUPTO = formatDateToDatePicker(data.REGISTRATIONUPTO);
                $scope.ProviderDetails.TELEPHONENUMBER = data.TELEPHONENUMBER;
                $scope.ProviderDetails.EMAIL = data.EMAIL;


            } else if ($scope.REGISTEREDENTITYTYPE = 'INDIVIDUAL') {
                $scope.ProviderDetails.DOCTORPRN = data.PRN;
                $scope.ProviderDetails.FIRSTNAME = data.FIRSTNAME;
                $scope.ProviderDetails.MIDDLENAME = data.MIDDLENAME;
                $scope.ProviderDetails.LASTNAME = data.LASTNAME;
                $scope.ProviderDetails.QUALIFICATION = data.QUALIFICATION;
                $scope.ProviderDetails.REGISTRATIONNUMBER = data.REGISTRATIONNUMBER;
                $scope.ProviderDetails.REGISTEREDCOUNCIL = data.REGISTEREDCOUNCIL;
                $scope.ProviderDetails.PHONENUMBER = data.MOBILENUMBER;
            }
            $('#PRNSearchModal').modal('hide');
            $scope.reset('providerSearch');
        }

        $scope.reset = function (name) {
            switch (name) {
                case 'providerSearch':
                    $scope.providerSearchData = {};
                    $scope.providerSearch = {};
                    break;
                case 'bankSearch':
                    $scope.bankDetailsData = {};
                    $scope.bankSearch.searchText = '';
                    break;
                case 'bankform':
                    $scope.BankDetails = {};
                    break;
                case 'providerDetailsForm':
                    break;

            }
        }

        $scope.saveProviderDetailsData = function (clickedButton) {

            if ($scope.form.ProviderDetailsForm.$invalid) {
                $scope.showWarningMessage("Invalid form data");
                $scope.form.ProviderDetailsForm.submitted = true;
                return;
            }
            $scope.ProviderDetails.INWARDID = $scope.ClaimInfo.INWARDID;
            $scope.ProviderDetails.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            $scope.ProviderDetails.REGISTRATIONVALIDUPTO = makeSQLServerComp($scope.ProviderDetails.REGISTRATIONVALIDUPTO);
            var input = {
                "providerDetailsInputType": $scope.ProviderDetails
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (!$scope.ProviderDetails.HEALTHPROVIDERDETAILSID)
                        $scope.ProviderDetails.HEALTHPROVIDERDETAILSID = data.output1.items[0].PROVIDERDETAILSID;

                    $scope.ProviderDetails.REGISTRATIONVALIDUPTO = makePickerComp($scope.ProviderDetails.REGISTRATIONVALIDUPTO);
                    $scope.showSuccessMessage("Record Inserted/Updated Successfully");
                    $scope.PrevOrNextTabSelection(clickedButton);
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Saving Provider Details Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveProviderDetailsData(serviceArgs);

        }


        // bank details
        // $scope.currentPosition = 1;
        // $scope.resultCount = 0;
        // $scope.totalPages = 0;
        // $scope.pageSize = 2;

        $scope.getBankDetails = function () {
            var input = {
                "searchText": $scope.bankSearch.searchText,
                "SearchParam": $scope.bankSearch.SearchParam,
                "tableName": "IFSCCODE",
                "startPosition": $scope.currentPosition - 1,
                "orderBy": "IFSC",
                "noOfRow": 7
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.searchResults.items.length > 0) {
                        $scope.bankDetailsData = data.searchResults.items;
                    }
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while featching bank details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getSearchData(serviceArgs);
        }

        $scope.setBankDetail = function (selectedData) {
            $scope.BankDetails.BANK = selectedData.BANK;
            $scope.BankDetails.BRANCH = selectedData.BRANCH;
            $scope.BankDetails.IFSC = selectedData.IFSC;
            $('#bankModal').modal('hide');
            $scope.reset('bankSearch');
        }

        $scope.saveBankDetails = function (clickedButton) {

            if (!(!!$scope.BankDetails.CHEQUEDDDETAILS || !!$scope.BankDetails.ACCOUNTNUMBER)) {
                $scope.showWarningMessage("Please enter either account number or  cheque/DDpayable details");
                $scope.form.BankForm.submitted = true;
                return;
            }

            if ($scope.form.BankForm.$invalid) {
                $scope.showWarningMessage("invalid form data");
                $scope.form.BankForm.submitted = true;
                return;
            }
            $scope.BankDetails.INWARDID = $scope.ClaimInfo.INWARDID;
            $scope.BankDetails.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            var input = {
                "bankDetailsInputType": $scope.BankDetails

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (!$scope.BankDetails.HEALTHBANKDETAILSID)
                        $scope.BankDetails.HEALTHBANKDETAILSID = data.PRIMARYID.items[0].BANKDETAILSID;
                    $scope.showSuccessMessage("Record Inserted/Updated Successfully");
                    $scope.decServiceCallCounter();
                    $scope.PrevOrNextTabSelection(clickedButton);

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while saving bank details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveBankDetails(serviceArgs);
        }
        //footer data
        $scope.getFooterData = function (clickedTag) {
            var input = {
                TAGNAME: clickedTag,
                INWARDID: $scope.ClaimInfo.INWARDID,
                FEATURESUMMARYID: $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    switch (clickedTag) {
                        case 'CLAIMFLAG':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'CLAIMHISTORY':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'POLICYDETAILS':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'NOTES':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'BSI':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'INWARDDETAILS':
                            $scope.inwardDetailsData = data.FOOOTERDATA.items;
                            $('#inwardDetailsModal').modal('show');
                            break;
                        case 'COMMUNICATIONSENT':
                            $scope.xyz = data.FOOOTERDATA.items;
                            break;
                        case 'DOCUMENTS':
                            $scope.documentsData = data.FOOOTERDATA.items;
                            $('#documentDetailsModal').modal('show');
                            break;
                        case 'PROVIDERDETAILS':
                            $scope.providerDetailsData = data.FOOOTERDATA.items[0];
                            break;
                        case 'TRANSACTION':
                            $scope.gctransactionData = data.FOOOTERDATA.items
                            $('#gctransactionModal').modal('show');
                            break

                    }
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while featching " + clickedTag);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getFooterData(serviceArgs);
        }

        $scope.downloadDocuments = function (HEALTHDOCUMENTMANAGEMENTID, OMNIDOCSID) {

            if (!!OMNIDOCSID) {
                $window.open("//www.google.com/");
                return;
            }

            var input = {
                "GetDocumentInput": {
                    "DocID": HEALTHDOCUMENTMANAGEMENTID,
                }
            }

            var serviceArgs = {
                params: JSON.stringify(input),

                load: function (data) {
                    var fileContent = 'data:application/octet-stream;base64,' + data.GetDocumentOutput.FileContent;
                    $("#DownloadedDocument").attr('href', fileContent);
                    var downloadFileName = data.GetDocumentOutput.FileName;
                    $("#DownloadedDocument").attr('download', downloadFileName);
                    $("#DownloadedDocument").get(0).click();
                    $scope.decServiceCallCounter();

                },

                error: function (e) {
                    $scope.decServiceCallCounter();
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getDocumentByID(serviceArgs);
        }
        //task ending

        $scope.endUserTask = function () {

            var input = {
                "taskId": $scope.context.options.TASKID.boundObject.TASKID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.context.trigger();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in task ending");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.endUserTask(serviceArgs);
        }

        /**********hospitalization details********************************************************************************/

        $scope.hospitalizationDetailsTabData = {};
        $scope.wrongDateOfDischarge = false;

        var modifyDate = function (date) {
            if (date != undefined && date != "") {
                if (date.indexOf('-') > 2)
                    return date;
                else
                    return date.split('-').reverse().join('-');
            }
            else
                return null;
        }


        $scope.$watchGroup(['hospitalizationDetailsTabData.DATEOFADMISSIONLOSS', 'hospitalizationDetailsTabData.DATEOFDISCHARGE'], function () {
            $scope.wrongDateOfDischarge = false;
            $scope.wrongDateOfDiseaseFirstDetected = false;
            $scope.wrongDateOfInjury = false;
            $scope.wrongDateOfDelivery = false;

            if ($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS != undefined && $scope.hospitalizationDetailsTabData.DATEOFDISCHARGE != undefined) {

                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEOFDISCHARGE = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE);
                $scope.hospitalizationDetailsTabData.LENGTHOFSTAY = (new Date(DATEOFDISCHARGE) - new Date(DATEOFADMISSIONLOSS)) / (24 * 60 * 60 * 1000);
                $scope.deviationMorethanTwo = false;
                if (new Date(DATEOFDISCHARGE) < new Date(DATEOFADMISSIONLOSS)) {
                    $scope.wrongDateOfDischarge = true;

                }

            }
            if ($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS != undefined && $scope.hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED != undefined) {

                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEDISEASEFIRSTDETECTED = modifyDate($scope.hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED);

                if (new Date(DATEOFADMISSIONLOSS) < new Date(DATEDISEASEFIRSTDETECTED)) {
                    $scope.wrongDateOfDiseaseFirstDetected = true;
                }
            }

            if ($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS != undefined && $scope.hospitalizationDetailsTabData.DATEOFINJURY != undefined) {

                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEOFINJURY = modifyDate($scope.hospitalizationDetailsTabData.DATEOFINJURY);

                if (new Date(DATEOFADMISSIONLOSS) < new Date(DATEOFINJURY)) {
                    $scope.wrongDateOfInjury = true;
                }
            }

            if ($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE != undefined && $scope.hospitalizationDetailsTabData.DATEOFDELIVERY != undefined) {

                var DATEOFDISCHARGE = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE);
                var DATEOFDELIVERY = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDELIVERY);

                if (new Date(DATEOFDELIVERY) < new Date(DATEOFDISCHARGE)) {
                    $scope.wrongDateOfDelivery = true;
                }
            }
        });

        $scope.$watch('hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED', function () {
            $scope.wrongDateOfDiseaseFirstDetected = false;
            if ($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS != undefined && $scope.hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED != undefined) {

                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEDISEASEFIRSTDETECTED = modifyDate($scope.hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED);

                if (new Date(DATEOFADMISSIONLOSS) < new Date(DATEDISEASEFIRSTDETECTED)) {
                    $scope.wrongDateOfDiseaseFirstDetected = true;
                }
            }
        });

        $scope.$watch('hospitalizationDetailsTabData.DATEOFINJURY', function () {
            $scope.wrongDateOfInjury = false;
            if ($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS != undefined && $scope.hospitalizationDetailsTabData.DATEOFINJURY != undefined) {

                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEOFINJURY = modifyDate($scope.hospitalizationDetailsTabData.DATEOFINJURY);

                if (new Date(DATEOFADMISSIONLOSS) < new Date(DATEOFINJURY)) {
                    $scope.wrongDateOfInjury = true;
                }
            }
        });


        $scope.$watch('hospitalizationDetailsTabData.DATEOFDELIVERY', function () {
            $scope.wrongDateOfDelivery = false;
            if ($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE != undefined && $scope.hospitalizationDetailsTabData.DATEOFDELIVERY != undefined) {

                var DATEOFDISCHARGE = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE);
                var DATEOFDELIVERY = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDELIVERY);

                if (new Date(DATEOFDELIVERY) < new Date(DATEOFDISCHARGE)) {
                    $scope.wrongDateOfDelivery = true;
                }
            }
        });

        $scope.$watch('hospitalizationDetailsTabData.HOSPITALIZATIONDUETO', function () {

            $scope.hospitalizationDetailsTabData.DATEDISEASEFIRSTDETECTED = undefined;
            $scope.hospitalizationDetailsTabData.AILMENT = undefined;
            $scope.hospitalizationDetailsTabData.DATEOFINJURY = undefined;
            $scope.hospitalizationDetailsTabData.INJURYDETAILS = undefined;
            $scope.hospitalizationDetailsTabData.CAUSE = undefined;
            $scope.hospitalizationDetailsTabData.ISMEDICOLEGAL = undefined;
            $scope.hospitalizationDetailsTabData.REPORTEDTOPOLICE = undefined;
            $scope.hospitalizationDetailsTabData.MLCREPORTANDPOLICEFIRATTACHED = undefined;
            $scope.hospitalizationDetailsTabData.REASONIFNOTREPORTEDTOPOLICE = undefined;
            $scope.hospitalizationDetailsTabData.FIRNUMBER = undefined;
            $scope.hospitalizationDetailsTabData.TESTCONDUCTEDTOESTABLISH = undefined;
            $scope.hospitalizationDetailsTabData.REPORTSATTACHED = undefined;
            $scope.hospitalizationDetailsTabData.DATEOFDELIVERY = undefined;
            $scope.hospitalizationDetailsTabData.GRAVIDA = undefined;
            $scope.hospitalizationDetailsTabData.PARITY = undefined;
            $scope.hospitalizationDetailsTabData.ABORTUS = undefined;
            $scope.hospitalizationDetailsTabData.LIVINGCHILDREN = undefined;
            $scope.wrongDateOfInjury = false;
            $scope.wrongDateOfDiseaseFirstDetected = false;
            $scope.wrongDateOfDelivery = false;
        });

        $scope.$watch('hospitalizationDetailsTabData.CAUSE', function () {

            $scope.hospitalizationDetailsTabData.ISMEDICOLEGAL = undefined;
            $scope.hospitalizationDetailsTabData.REPORTEDTOPOLICE = undefined;
            $scope.hospitalizationDetailsTabData.MLCREPORTANDPOLICEFIRATTACHED = undefined;
            $scope.hospitalizationDetailsTabData.REASONIFNOTREPORTEDTOPOLICE = undefined;
            $scope.hospitalizationDetailsTabData.FIRNUMBER = undefined;
            $scope.hospitalizationDetailsTabData.TESTCONDUCTEDTOESTABLISH = undefined;
            $scope.hospitalizationDetailsTabData.REPORTSATTACHED = undefined;

        });

        $scope.validateLengthOfStay = function () {
            if ($scope.hospitalizationDetailsTabData.LENGTHOFSTAY == undefined) {
                $scope.deviationMorethanTwo = false;
                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEOFDISCHARGE = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE);
                $scope.hospitalizationDetailsTabData.LENGTHOFSTAY = (new Date(DATEOFDISCHARGE) - new Date(DATEOFADMISSIONLOSS)) / (24 * 60 * 60 * 1000);
            }
            else {
                var DATEOFADMISSIONLOSS = modifyDate($scope.hospitalizationDetailsTabData.DATEOFADMISSIONLOSS);
                var DATEOFDISCHARGE = modifyDate($scope.hospitalizationDetailsTabData.DATEOFDISCHARGE);
                var length = (new Date(DATEOFDISCHARGE) - new Date(DATEOFADMISSIONLOSS)) / (24 * 60 * 60 * 1000);
                if (length > $scope.hospitalizationDetailsTabData.LENGTHOFSTAY) {
                    if (length - $scope.hospitalizationDetailsTabData.LENGTHOFSTAY > 2)
                        $scope.deviationMorethanTwo = true;
                    else
                        $scope.deviationMorethanTwo = false;
                }
                else {
                    if ($scope.hospitalizationDetailsTabData.LENGTHOFSTAY - length > 2)
                        $scope.deviationMorethanTwo = true;
                    else
                        $scope.deviationMorethanTwo = false;
                }
            }
        }

        $scope.$evalAsync(function () {

            $scope.saveHospitalizationDetails = function (clickedButton) {
                debugger;

                if ($scope.form.HospitalizationDetails.$valid && !$scope.wrongDateOfDischarge && !$scope.deviationMorethanTwo) {
                    if ($scope.hospitalizationDetailsTabData.HOSPITALIZATIONDUETO == "Injury" && !$scope.wrongDateOfInjury) {
                        $scope.saveToHospitalizationDetailsDB(clickedButton);
                        $scope.form.HospitalizationDetails.submitted = false;
                    }
                    if ($scope.hospitalizationDetailsTabData.HOSPITALIZATIONDUETO == "Illness" && !$scope.wrongDateOfDiseaseFirstDetected) {
                        $scope.saveToHospitalizationDetailsDB(clickedButton);
                        $scope.form.HospitalizationDetails.submitted = false;
                    }
                    if ($scope.hospitalizationDetailsTabData.HOSPITALIZATIONDUETO == "Maternity" && !$scope.wrongDateOfDelivery) {
                        $scope.saveToHospitalizationDetailsDB(clickedButton);
                        $scope.form.HospitalizationDetails.submitted = false;
                    }
                }
                else {
                    $scope.form.HospitalizationDetails.submitted = true;
                }
            }
        });

        $scope.saveToHospitalizationDetailsDB = function (clickedButton) {
            var temp = angular.copy($scope.hospitalizationDetailsTabData);

            temp.DATEOFADMISSIONLOSS = modifyDate(temp.DATEOFADMISSIONLOSS);
            temp.DATEOFDISCHARGE = modifyDate(temp.DATEOFDISCHARGE);
            temp.DATEDISEASEFIRSTDETECTED = modifyDate(temp.DATEDISEASEFIRSTDETECTED);
            temp.DATEOFINJURY = modifyDate(temp.DATEOFINJURY);
            temp.DATEOFDELIVERY = modifyDate(temp.DATEOFDELIVERY);

            if (temp.DATEOFADMISSIONLOSSTIME == undefined)
                temp.DATEOFADMISSIONLOSSTIME = null;
            else
                temp.DATEOFADMISSIONLOSSTIME = moment(temp.DATEOFADMISSIONLOSSTIME).format("HH:mm");

            if (temp.DATEOFDISCHARGETIME == undefined)
                temp.DATEOFDISCHARGETIME = null;
            else
                temp.DATEOFDISCHARGETIME = moment(temp.DATEOFDISCHARGETIME).format("HH:mm");

            temp.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            var input = {
                "hospitalizationDetailsInputType": temp

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (!$scope.hospitalizationDetailsTabData.HEALTHHOSPITALIZATIONDETAILSID)
                        $scope.hospitalizationDetailsTabData.HEALTHHOSPITALIZATIONDETAILSID = data.OUTPUTID.items[0].HOSPITALIZATIONDETAILSID;
                    $scope.showSuccessMessage("Record Inserted/Updated Successfully");
                    //$scope.resetHospitalizationDetails();
                    $scope.decServiceCallCounter();
                    $scope.PrevOrNextTabSelection(clickedButton);

                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while saving hospitalization details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveHospitalizationDetails(serviceArgs);
        }



        $scope.resetHospitalizationDetails = function () {
            $scope.form.HospitalizationDetails.$setPristine();
            $scope.form.HospitalizationDetails.$setUntouched();
            $("#HospitalizationDetailsForm")[0].reset();
            $scope.hospitalizationDetailsTabData = {};
            $scope.form.HospitalizationDetails.submitted = false;
            $scope.deviationMorethanTwo = false;
            $scope.wrongDateOfDischarge = false;
        }


/**intimation Master start**/
      
        $scope.openIntimationModal=function()
        {
            if ($scope.insuredPersonData.MEMBERID == "" || $scope.insuredPersonData.MEMBERID==undefined) 
                $scope.showWarningMessage("Member ID is missing !!");
            else {
                $scope.getintimationList(1);

                $('#intimationModal').modal({
                    backdrop: 'static'
                });
            }
        }

        $scope.intimationTableData = false;

        $(document).ready(function () {
            $scope.getintimationList = function (pageStart) {
                $scope.intimationCurrentPosition = pageStart;

                var input;
                debugger;

                input = {
                    "startPosition": pageStart - 1, "memberID": $scope.insuredPersonData.MEMBERID
                };

                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        debugger;
                        if (data.result.items.length > 0) {
                            $scope.intimationTableData = false;
                            $scope.intimationList = data.result.items;
                            $scope.intimationResultCount = data.resultCount.items[0].RECORDCOUNT;
                            $scope.assignPagVariablesTointimation();

                        } else {
                            $scope.intimationList = {};
                            $scope.intimationTableData = true;
                            $scope.totalintimationPages = 0;
                        }
                        $scope.$apply();
                    },

                    error: function (e) {

                        $scope.showErrorMessage(e, "Error while Getting intimation Data");
                    }
                };
                $scope.context.options.getintimationMaster(serviceArgs);

            }


            $scope.intimationCurrentPosition = 1;
            $scope.intimationResultCount = 0;
            $scope.totalintimationPages = 0;
            var pageSize = 10;

            $scope.assignPagVariablesTointimation = function () {

                $scope.totalintimationPages = Math.ceil($scope.intimationResultCount / pageSize);

                switch ($scope.totalintimationPages) {

                    case 1:
                        $scope.intimationPreviousPage = 0;
                        $scope.intimationNextPage = 0;
                        $scope.intimationMiddlePage = 1;
                        break;
                    case 2:
                        if ($scope.intimationCurrentPosition == 1) {
                            $scope.intimationNextPage = 2;
                            $scope.intimationPreviousPage = 0;

                        } else if ($scope.intimationCurrentPosition == 2) {
                            $scope.intimationPreviousPage = 1;
                            $scope.intimationNextPage = 0;
                        }
                        $scope.intimationMiddlePage = $scope.intimationCurrentPosition;
                        break;
                    default:
                        if ($scope.intimationCurrentPosition == 1) {

                            $scope.intimationPreviousPage = 1;
                            $scope.intimationMiddlePage = $scope.intimationCurrentPosition + 1;
                            $scope.intimationNextPage = $scope.intimationCurrentPosition + 2;

                        } else if ($scope.intimationCurrentPosition == $scope.totalintimationPages) {

                            $scope.intimationPreviousPage = $scope.intimationCurrentPosition - 2;
                            $scope.intimationMiddlePage = $scope.intimationCurrentPosition - 1;
                            $scope.intimationNextPage = $scope.intimationCurrentPosition;

                        } else {
                            $scope.intimationPreviousPage = $scope.intimationCurrentPosition - 1;
                            $scope.intimationMiddlePage = $scope.intimationCurrentPosition;
                            $scope.intimationNextPage = $scope.intimationCurrentPosition + 1;
                        }

                }

                $scope.$apply();

            }

        });

        $scope.resetIntimationFilter = function () {
            $scope.intimationTableData = false;
            $scope.intimationResultCount = 0;
            $scope.totalintimationPages = 0;
        }

        $scope.setintimation = function (item) {
            var index = $scope.intimationList.indexOf(item);
            var i = JSON.parse(JSON.stringify($scope.intimationList[index]));
            $scope.hospitalizationDetailsTabData.INTIMATIONID = i.INTIMATIONID;
            $scope.resetIntimationFilter();
            $('#intimationModal').modal('hide');
        }

  
  
/**Intimation Master end**/

/*************************hospitalization details end**************************************************************/
/******************************************invoice details start*****************************************/
        $scope.invoiceTabData = {};
        $scope.invoiceTabData.SERVICEDETAILROW = [];
        $scope.invoiceTabData.SERVICEDETAILROW[0] = {};
        $scope.addInvoice = true;
        $scope.VIEWINVOICECHECKBOX = [];
        $scope.viewInvoiceOnly = false;

        $scope.viewinvoice = function () {
            $scope.viewInvoiceOnly = true;
            $scope.setinvoice();
        }
        $scope.editinvoice = function () {
            $scope.viewInvoiceOnly = false;
            $scope.setinvoice();
        }

        $scope.addRowToServiceDetails = function () {
            if ($scope.invoiceTabData.SERVICEDETAILROW) {
                var tl = $scope.invoiceTabData.SERVICEDETAILROW.length;
                $scope.invoiceTabData.SERVICEDETAILROW[tl] = {};
                $scope.invoiceTabData.SERVICEDETAILROW[tl].CATEGORYLEVEL4 = $scope.categoryLevel4Options[0];
                if ($scope.invoiceTabData.ISTARIFFCONFIGURED) {
                    $scope.invoiceTabData.SERVICEDETAILROW[tl].ITEMDESCRIPTION = $scope.itemDescOptions[0];
                    $scope.invoiceTabData.SERVICEDETAILROW[tl].ROOMTYPE = $scope.roomTypeOptions[0];
                }

            }
            else {
                $scope.invoiceTabData.SERVICEDETAILROW = [];
                $scope.invoiceTabData.SERVICEDETAILROW[0] = {};
                $scope.invoiceTabData.SERVICEDETAILROW[0].CATEGORYLEVEL4 = $scope.categoryLevel4Options[0];
                if ($scope.invoiceTabData.ISTARIFFCONFIGURED) {
                    $scope.invoiceTabData.SERVICEDETAILROW[0].ITEMDESCRIPTION = $scope.itemDescOptions[0];
                    $scope.invoiceTabData.SERVICEDETAILROW[0].ROOMTYPE = $scope.roomTypeOptions[0];
                }
            }
        }
        $scope.deleteRowFromServiceDetails = function (index) {

            if ($scope.invoiceTabData.HEALTHINVOICEID)
                $scope.deleteService(index);
            else
                $scope.invoiceTabData.SERVICEDETAILROW.splice(index, 1);

            $scope.calculateTotalAmounts();
        }
        $scope.$watch('invoiceTabData.PARTYISHOSPITAL', function () {
            if ($scope.invoiceTabData.PARTYISHOSPITAL == 'true')
                $scope.invoiceTabData.PARTYNAME = $scope.ProviderDetails.PROVIDERNAME;

            $scope.callProviderTariffDetails();
        });

        $scope.itemDescOptions = ["--SELECT--"];
        $scope.categoryLevel4Options = ["--SELECT--"];
        $scope.roomTypeOptions = ["--SELECT--"];
        $scope.invoiceTabData.TOTALGROSSAMOUNT = 0;
        $scope.invoiceTabData.TOTALNETAMOUNT = 0;
        $scope.invoiceTabData.SERVICEDETAILROW[0].CATEGORYLEVEL4 = "--SELECT--";

        $scope.calculateNetAmount=function(index){
    if(!$scope.invoiceTabData.SERVICEDETAILROW[index].UNIT)
        $scope.invoiceTabData.SERVICEDETAILROW[index].UNIT=1;
    if(!$scope.invoiceTabData.SERVICEDETAILROW[index].DISCOUNTASPERINVOICE &&  $scope.invoiceTabData.SERVICEDETAILROW[index].DISCOUNTASPERINVOICE!=0)
    $scope.invoiceTabData.SERVICEDETAILROW[index].DISCOUNTASPERINVOICE=1;

            if ($scope.invoiceTabData.SERVICEDETAILROW[index].GROSSAMOUNT != undefined && $scope.invoiceTabData.SERVICEDETAILROW[index].DISCOUNTASPERINVOICE != undefined) {

                $scope.invoiceTabData.SERVICEDETAILROW[index].NETAMOUNT = $scope.invoiceTabData.SERVICEDETAILROW[index].GROSSAMOUNT * (1 - ($scope.invoiceTabData.SERVICEDETAILROW[index].DISCOUNTASPERINVOICE / 100));

                $scope.invoiceTabData.SERVICEDETAILROW[index].NETAMOUNTPERUNIT = $scope.invoiceTabData.SERVICEDETAILROW[index].NETAMOUNT / $scope.invoiceTabData.SERVICEDETAILROW[index].UNIT;
            }

            $scope.calculateTotalAmounts();
        }

        $scope.calculateTotalAmounts = function () {

            var l = $scope.invoiceTabData.SERVICEDETAILROW.length;
            if (l > 0) {
                $scope.invoiceTabData.TOTALGROSSAMOUNT = 0;
                $scope.invoiceTabData.TOTALNETAMOUNT = 0;
                for (var i = 0; i < l; i++) {
                    if ($scope.invoiceTabData.SERVICEDETAILROW[i].GROSSAMOUNT != undefined)
                        $scope.invoiceTabData.TOTALGROSSAMOUNT += parseFloat($scope.invoiceTabData.SERVICEDETAILROW[i].GROSSAMOUNT);
                    if ($scope.invoiceTabData.SERVICEDETAILROW[i].NETAMOUNT != undefined)
                        $scope.invoiceTabData.TOTALNETAMOUNT += parseFloat($scope.invoiceTabData.SERVICEDETAILROW[i].NETAMOUNT);
                }
            }
        }

        $scope.saveInvoiceDetails = function (clickedButton) {

            if ($scope.form.InvoiceDetails.$valid) {
                $scope.saveInvoiceDetailsToDb(clickedButton);
                $scope.form.InvoiceDetails.submitted = false;
            }
            else
                $scope.form.InvoiceDetails.submitted = true;
        }

        $scope.saveInvoiceDetailsToDb = function (clickedButton) {
            var tempInvoiceData = angular.copy($scope.invoiceTabData);
            tempInvoiceData.INVOICEDATE = modifyDate(tempInvoiceData.INVOICEDATE);
            tempInvoiceData.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;

            var tempServiceData = angular.copy($scope.invoiceTabData.SERVICEDETAILROW);
            delete tempInvoiceData["SERVICEDETAILROW"];
            delete tempInvoiceData["ISTARIFFCONFIGURED"];
            for (var i = 0; i < tempServiceData.length; i++) {
                delete tempServiceData[i]["$$hashKey"];
                tempServiceData[i].FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
                tempServiceData[i].INVOICENUMBER = angular.copy($scope.invoiceTabData.INVOICENUMBER);
            }


            var input = {
                "invoiceInputType": tempInvoiceData,
                "serviceInputType": tempServiceData

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    if (data.invoiceId.items.length > 0) 
                    {
                        if (data.invoiceId.items[0].TOTALCOUNT > 0)
                            $scope.showWarningMessage('Duplicate Invoice Number!!');
                        else 
                        {
                            $scope.showSuccessMessage("Record Inserted Successfully");
                            $scope.callBillingFunction('getAllServices');
                            $scope.PrevOrNextTabSelection(clickedButton);
                        }
                    }
                    else 
                    {
                        $scope.showSuccessMessage("Record Updated Successfully");
                        $scope.callBillingFunction('getAllServices');
                        $scope.PrevOrNextTabSelection(clickedButton);
                    }

                    $scope.decServiceCallCounter();


                },
                error: function (e) {

                    $scope.showErrorMessage(e, "Error while saving invoice details");
                    $scope.decServiceCallCounter();
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveInvoiceDetails(serviceArgs);
        }

        $scope.resetInvoiceDetails = function () {
            $scope.form.InvoiceDetails.$setPristine();
            $scope.form.InvoiceDetails.$setUntouched();
            $("#InvoiceDetailsForm")[0].reset();
            $scope.invoiceTabData = {};
            $scope.form.InvoiceDetails.submitted = false;
            $scope.invoiceTabData.SERVICEDETAILROW = [];
            $scope.invoiceTabData.SERVICEDETAILROW[0] = {};
            $scope.invoiceTabData.SERVICEDETAILROW[0].CATEGORYLEVEL4 = "--SELECT--";
            $scope.addInvoice = true;
            $scope.invoiceTabData.TOTALGROSSAMOUNT = 0;
            $scope.invoiceTabData.TOTALNETAMOUNT = 0;
            $scope.viewInvoiceOnly = false;
            $scope.invoiceTabData.CURRENCY = "INR";
            $scope.invoiceTabData.EXCHANGERATE = 1;
        }

        /***************************invoice modal start****************************/
        var PARTYNAMECHANGED = false;
        $scope.viewInvoiceTableData = false;

        $(document).ready(function () {
            $scope.getinvoiceList = function (pageStart) {

                $scope.VIEWINVOICECHECKBOX = [];
                $scope.viewInvoiceCurrentPosition = pageStart;

                var input;
                debugger;
                input = {
                    "startPosition": pageStart - 1, "pageSize": $scope.pageSize
                };

                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        debugger;
                        if (data.result.items.length > 0) {
                            $scope.viewInvoiceTableData = false;
                            $scope.viewInvoiceList = data.result.items;
                            $scope.viewInvoiceResultCount = data.resultCount.items[0].RECORDCOUNT;
                            $scope.assignPagVariablesToinvoice();

                        } else {
                            $scope.viewInvoiceList = {};
                            $scope.viewInvoiceTableData = true;
                            $scope.totalviewInvoicePages = 0;
                        }
                        $scope.$apply();
                        $scope.decServiceCallCounter();
                    },

                    error: function (e) {

                        $scope.showErrorMessage(e, "Error while Getting invoice Data");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getinvoiceMaster(serviceArgs);

            }


            $scope.viewInvoiceCurrentPosition = 1;
            $scope.viewInvoiceResultCount = 0;
            $scope.totalviewInvoicePages = 0;
            $scope.pageSize = 5;

            $scope.assignPagVariablesToinvoice = function () {

                $scope.totalviewInvoicePages = Math.ceil($scope.viewInvoiceResultCount / $scope.pageSize);

                switch ($scope.totalviewInvoicePages) {

                    case 1:
                        $scope.viewInvoicePreviousPage = 0;
                        $scope.viewInvoiceNextPage = 0;
                        $scope.viewInvoiceMiddlePage = 1;
                        break;
                    case 2:
                        if ($scope.viewInvoiceCurrentPosition == 1) {
                            $scope.viewInvoiceNextPage = 2;
                            $scope.viewInvoicePreviousPage = 0;

                        } else if ($scope.viewInvoiceCurrentPosition == 2) {
                            $scope.viewInvoicePreviousPage = 1;
                            $scope.viewInvoiceNextPage = 0;
                        }
                        $scope.viewInvoiceMiddlePage = $scope.viewInvoiceCurrentPosition;
                        break;
                    default:
                        if ($scope.viewInvoiceCurrentPosition == 1) {

                            $scope.viewInvoicePreviousPage = 1;
                            $scope.viewInvoiceMiddlePage = $scope.viewInvoiceCurrentPosition + 1;
                            $scope.viewInvoiceNextPage = $scope.viewInvoiceCurrentPosition + 2;

                        } else if ($scope.viewInvoiceCurrentPosition == $scope.totalviewInvoicePages) {

                            $scope.viewInvoicePreviousPage = $scope.viewInvoiceCurrentPosition - 2;
                            $scope.viewInvoiceMiddlePage = $scope.viewInvoiceCurrentPosition - 1;
                            $scope.viewInvoiceNextPage = $scope.viewInvoiceCurrentPosition;

                        } else {
                            $scope.viewInvoicePreviousPage = $scope.viewInvoiceCurrentPosition - 1;
                            $scope.viewInvoiceMiddlePage = $scope.viewInvoiceCurrentPosition;
                            $scope.viewInvoiceNextPage = $scope.viewInvoiceCurrentPosition + 1;
                        }

                }

                $scope.$apply();

            }

        });

        $scope.resetInvoiceFilter = function () {
            $scope.viewInvoiceTableData = false;
            $scope.viewInvoiceResultCount = 0;
            $scope.totalviewInvoicePages = 0;
        }

        $scope.setinvoice = function () {

            var temp = 0;
            var pos = 0;
            for (var i = 0; i < $scope.VIEWINVOICECHECKBOX.length; i++) {
                if (temp > 1)
                    break;
                if ($scope.VIEWINVOICECHECKBOX[i] != undefined)
                    if ($scope.VIEWINVOICECHECKBOX[i].VALUE == true) { temp++; pos = i; }
            }
            if (temp != 1)
                $scope.showErrorMessage(e, "Select one invoice.");
            else {
                var serviceData = JSON.parse(JSON.stringify($scope.viewInvoiceList[pos]));
                $scope.invoiceTabData.INVOICENUMBER = serviceData.INVOICENUMBER;
                var d = serviceData.INVOICEDATE.toString();
                var d = d.substring(0, d.indexOf('T'));
                $scope.invoiceTabData.INVOICEDATE = modifyDate(d);
                $scope.invoiceTabData.PINCODE = serviceData.PINCODE;
                $scope.invoiceTabData.PARTYISHOSPITAL = serviceData.PARTYISHOSPITAL;
                $scope.invoiceTabData.PARTYNAME = serviceData.PARTYNAME;
                $scope.invoiceTabData.CURRENCY = serviceData.CURRENCY;
                $scope.invoiceTabData.EXCHANGERATE = parseFloat(serviceData.EXCHANGERATE);
                $scope.invoiceTabData.TOTALGROSSAMOUNT = serviceData.TOTALGROSSAMOUNT;
                $scope.invoiceTabData.TOTALNETAMOUNT = serviceData.TOTALNETAMOUNT;
                $scope.invoiceTabData.HEALTHINVOICEID = serviceData.HEALTHINVOICEID.toString();

                $scope.callProviderTariffDetails();
                $scope.getServices(serviceData);

            }

        }

        $scope.getServices = function (serviceData) {
            var input = {
                "invoicenumber": serviceData.INVOICENUMBER, "featuresummaryid": serviceData.FEATURESUMMARYID
            };
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    var t = data.output1.items;
                    $scope.invoiceTabData.SERVICEDETAILROW = [];
                    for (var i = 0; i < t.length; i++) {
                        $scope.invoiceTabData.SERVICEDETAILROW[i] = {};

                        if (t[i].ITEMDESCRIPTION) {
                            if ($scope.invoiceTabData.ISTARIFFCONFIGURED)
                                $scope.invoiceTabData.SERVICEDETAILROW[i].ITEMDESCRIPTION = $scope.itemDescOptions[$scope.itemDescOptions.indexOf(t[i].ITEMDESCRIPTION.trim())];
                            else
                                $scope.invoiceTabData.SERVICEDETAILROW[i].ITEMDESCRIPTION = t[i].ITEMDESCRIPTION.trim();
                        }
                        if (t[i].CATEGORYLEVEL4) {
                            if ($scope.invoiceTabData.ISTARIFFCONFIGURED)
                                $scope.invoiceTabData.SERVICEDETAILROW[i].CATEGORYLEVEL4 = $scope.categoryLevel4Options[$scope.categoryLevel4Options.indexOf(t[i].CATEGORYLEVEL4.trim())];
                            else
                                $scope.invoiceTabData.SERVICEDETAILROW[i].CATEGORYLEVEL4 = t[i].CATEGORYLEVEL4.trim();
                        }
                        $scope.invoiceTabData.SERVICEDETAILROW[i].CATEGORYLEVEL1 = t[i].CATEGORYLEVEL1;
                        if (t[i].ROOMTYPE) {
                            if ($scope.invoiceTabData.ISTARIFFCONFIGURED)
                                $scope.invoiceTabData.SERVICEDETAILROW[i].ROOMTYPE = $scope.roomTypeOptions[$scope.roomTypeOptions.indexOf(t[i].ROOMTYPE.trim())];
                            else
                                $scope.invoiceTabData.SERVICEDETAILROW[i].ROOMTYPE = t[i].ROOMTYPE;
                        }
                        $scope.invoiceTabData.SERVICEDETAILROW[i].UNIT = t[i].UNIT;
                        $scope.invoiceTabData.SERVICEDETAILROW[i].GROSSAMOUNT = parseFloat(t[i].GROSSAMOUNT);
                        $scope.invoiceTabData.SERVICEDETAILROW[i].DISCOUNTASPERINVOICE = parseFloat(t[i].DISCOUNTASPERINVOICE);
                        $scope.invoiceTabData.SERVICEDETAILROW[i].NETAMOUNT = t[i].NETAMOUNT;
                        $scope.invoiceTabData.SERVICEDETAILROW[i].NETAMOUNTPERUNIT = t[i].NETAMOUNTPERUNIT;
                        $scope.invoiceTabData.SERVICEDETAILROW[i].HEALTHINVOICESERVICEID = t[i].HEALTHINVOICESERVICEID.toString();
                    }
                    $scope.decServiceCallCounter();

                },
                error: function (e) {
                    console.log("error occured while fetching services list.");
                    $scope.decServiceCallCounter();
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getInvoiceServices(serviceArgs);
        }

        $scope.deleteService = function (index) {

            var ser = [];


            if (PARTYNAMECHANGED)
                ser = angular.copy($scope.invoiceTabData.SERVICEDETAILROW);
            else
                ser[0] = angular.copy($scope.invoiceTabData.SERVICEDETAILROW[index].HEALTHINVOICESERVICEID);

            var input = { "serviceID": ser };
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.invoiceTabData.SERVICEDETAILROW.splice(index, 1);
                    $scope.showSuccessMessage("Service deleted successfully.");
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error occured while deleting service.");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.deleteInvoiceServices(serviceArgs);
        }

        $scope.deleteInvoice = function () {
            var inv = [];
            var index = 0;
            for (var i = 0; i < $scope.VIEWINVOICECHECKBOX.length; i++) {
                if ($scope.VIEWINVOICECHECKBOX[i] != undefined)
                    if ($scope.VIEWINVOICECHECKBOX[i].VALUE == true)
                        inv[index++] = $scope.viewInvoiceList[i].HEALTHINVOICEID.toString();
            }

            var input = { "invoiceID": inv };

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    var j = 0;
                    for (var i = 0; i < $scope.VIEWINVOICECHECKBOX.length; i++) {
                        if ($scope.VIEWINVOICECHECKBOX[i] != undefined) {
                            if ($scope.VIEWINVOICECHECKBOX[i].VALUE == true)
                                $scope.viewInvoiceList.splice(j, 1);
                            else
                                j++;
                        }
                        else
                            j++;
                    }
                    $scope.VIEWINVOICECHECKBOX = [];
                    $scope.showSuccessMessage("Invoice deleted successfully");
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error occured while deleting invoice.");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.deleteInvoice(serviceArgs);
        }

        /***************************invoice modal end*****************************/

        /***************************tariff-provider modal start*******************/
        var providerTariffData = [];
        $scope.invoiceTabData.ISTARIFFCONFIGURED = false;



        var getProviderTariffDetails = function () {
            $scope.invoiceTabData.ISTARIFFCONFIGURED = false;
            $scope.itemDescOptions = ["--SELECT--"];
            $scope.categoryLevel4Options = ["--SELECT--"];
            providerTariffData = [];
            $scope.roomTypeOptions = ["--SELECT--"];
            $scope.invoiceTabData.SERVICEDETAILROW = [];
            $scope.invoiceTabData.SERVICEDETAILROW[0] = {};

            var input = { "providerName": $scope.invoiceTabData.PARTYNAME };

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.isTariffConfigured) {
                        if (data.isTariffConfigured.items[0].RECORDCOUNT > 0)
                            $scope.invoiceTabData.ISTARIFFCONFIGURED = true;
                    }
                    if (data.categories) {
                        providerTariffData = data.categories.items;
                        for (var i = 0; i < data.categories.items.length; i++) {
                            if (data.categories.items[i].HOSPITALSERVICE)
                                $scope.itemDescOptions[i + 1] = data.categories.items[i].HOSPITALSERVICE.trim();
                            if (data.categories.items[i].SERVICECATEGORY)
                                $scope.categoryLevel4Options[i + 1] = data.categories.items[i].SERVICECATEGORY.trim();

                        }
                    }
                    if (data.roomType) {
                        for (var i = 0; i < data.roomType.items.length; i++) {
                            if (data.roomType.items[i].ROOMTYPE)
                                $scope.roomTypeOptions[i + 1] = data.roomType.items[i].ROOMTYPE.trim();
                        }
                    }
                    if (!$scope.invoiceTabData.HEALTHINVOICEID) {
                        $scope.invoiceTabData.SERVICEDETAILROW[0].CATEGORYLEVEL4 = "--SELECT--";
                        if ($scope.invoiceTabData.ISTARIFFCONFIGURED) {
                            $scope.invoiceTabData.SERVICEDETAILROW[0].ITEMDESCRIPTION = "--SELECT--";
                            $scope.invoiceTabData.SERVICEDETAILROW[0].ROOMTYPE = "--SELECT--";
                        }
                    }

                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    console.log("error occured while fetching provider-tariff data: " + e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getTariffDetailsForProvider(serviceArgs);
        };

        $scope.callProviderTariffDetails = function () {
            //if(!$scope.invoiceTabData.HEALTHINVOICEID)
            getProviderTariffDetails();
        }

        $scope.mapCategories = function (index) {

            if ($scope.invoiceTabData.ISTARIFFCONFIGURED == true) {
                var i = $scope.itemDescOptions.indexOf($scope.invoiceTabData.SERVICEDETAILROW[index].ITEMDESCRIPTION);
                $scope.invoiceTabData.SERVICEDETAILROW[index].CATEGORYLEVEL4 = $scope.categoryLevel4Options[i];
                $scope.mapCategory1(index);

            }
        };

        $scope.mapCategory1 = function (index) {
            var i = $scope.categoryLevel4Options.indexOf($scope.invoiceTabData.SERVICEDETAILROW[index].CATEGORYLEVEL4);
            $scope.invoiceTabData.SERVICEDETAILROW[index].CATEGORYLEVEL1 = providerTariffData[i].tablename;
        };
        /***************************tariff-provider modal end*******************/
        /**currency modal start**/
        $scope.currencyOptions = [];
        $scope.invoiceTabData.CURRENCY = "INR";
        $scope.invoiceTabData.EXCHANGERATE = 1;
        var getcurrencyList = function () {

            var input = {};
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.currencyList.items) {
                        for (var i = 0; i < data.currencyList.items.length; i++) {
                            $scope.currencyOptions[i] = data.currencyList.items[i].CURRENCY;
                        }
                    }

                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.showErrorMessage(e, "Error occured while fetching currency list.");
                    $scope.decServiceCallCounter();
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getCurrency(serviceArgs);
        }

        $scope.openCurrencyModal = function () {

            getcurrencyList();
            $('#currencyModal').modal({
                backdrop: 'static'
            });
        }

        $scope.validateCurrency = function () {
            if ($scope.invoiceTabData.EXCHANGERATE && $scope.invoiceTabData.EXCHANGERATE > 0)
                $('#currencyModal').modal('hide');
        }

        /**currency modal end**/

        /******************************************invoice details end*****************************************/
/*****************************************************billing details start**************************************/
$scope.billingTabData={};
$scope.billingTabData.roomAndNursingCharges=[];
$scope.billingTabData.ICUcharges=[];
$scope.billingTabData.OTcharges=[];
$scope.billingTabData.medicinesAndConsumableCharges=[];
$scope.billingTabData.professionalFeesCharges=[];
$scope.billingTabData.investigationCharges=[];
$scope.billingTabData.ambulanceCharges=[];
$scope.billingTabData.miscellaneousCharges=[];
$scope.billingTabData.packageCharges=[];
$scope.CATEGORYLEVEL1OFMODAL="";
$scope.INVOICEMODALDATALINK={};
$scope.TARIFFMODALDATALINK={};
$scope.PROPORTIONATEDEDUCTIONDATALINK={};
$scope.PERUNITPOLICYLIMITSDATALINK={};
$scope.COPAYDATALINK={};
$scope.OVERRIDEDATALINK={};
var currentIndexOfParticularPanel=-1;

var getAllServices = function(){

    var input={"featureSummaryId":$scope.ClaimInfo.FEATURESUMMARYID};

    var serviceArgs={
        params:JSON.stringify(input),
        load:function(data){
            $scope.decServiceCallCounter();
            if(data.output.items)
            {
                var a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0,i=0;
                for(var i=0;i<data.output.items.length;i++)
                    {
                        switch(data.output.items[i].CATEGORYLEVEL1)
                        {
                            case  'Room and Nursing charges' : 
                                $scope.billingTabData.roomAndNursingCharges[a]={};
                                $scope.billingTabData.roomAndNursingCharges[a++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Ambulance charges' : 
                                $scope.billingTabData.ambulanceCharges[b]={};
                                $scope.billingTabData.ambulanceCharges[b++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'ICU charges' : 
                                $scope.billingTabData.ICUcharges[c]={};
                                $scope.billingTabData.ICUcharges[c++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'OT charges' : 
                                $scope.billingTabData.OTcharges[d]={};
                                $scope.billingTabData.OTcharges[d++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Medicines and consumable charges' : 
                                $scope.billingTabData.medicinesAndConsumableCharges[e]={};
                                $scope.billingTabData.medicinesAndConsumableCharges[e++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Professional fees charges' : 
                                $scope.billingTabData.professionalFeesCharges[f]={};
                                $scope.billingTabData.professionalFeesCharges[f++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Investigation charges' : 
                                $scope.billingTabData.investigationCharges[g]={};
                                $scope.billingTabData.investigationCharges[g++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Package charges' : 
                                $scope.billingTabData.packageCharges[h]={};
                                $scope.billingTabData.packageCharges[h++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            case  'Miscellaneous charges' : 
                                $scope.billingTabData.miscellaneousCharges[i]={};
                                $scope.billingTabData.miscellaneousCharges[i++].INVOICEDETAILS=data.output.items[i];
                                break ;
                            default : 
                                break ;
                        }
                    }
            }
        },
        error:function(e){
            $scope.decServiceCallCounter();
            $scope.showErrorMessage(e,"Error occured while fetching services for billing.");
        }
    }
    $scope.incServiceCallCounter();
    $scope.context.options.getAllServicesForBilling(serviceArgs);
};

var addRowToManualDisallowance =function(){
    if($scope.MANUALDISDATALINK.ROWS)
    $scope.MANUALDISDATALINK.ROWS[$scope.MANUALDISDATALINK.ROWS.length]={};
    else{
        $scope.MANUALDISDATALINK.ROWS=[];
        $scope.MANUALDISDATALINK.ROWS[0]={};
    }
}
var deleteRowFromManualDisallowance=function(index){
    $scope.MANUALDISDATALINK.ROWS.splice(index,1);
}
var copyManualDisallowanceData=function(category1,index)
{
    switch(category1)
    {
        case'Room and Nursing charges':
        $scope.billingTabData.roomAndNursingCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'ICU charges':
        $scope.billingTabData.ICUcharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'OT charges':
        $scope.billingTabData.OTcharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Medicines and consumable charges':
        $scope.billingTabData.medicinesAndConsumableCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Professional fees charges':
        $scope.billingTabData.professionalFeesCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Investigation charges':
        $scope.billingTabData.investigationCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Ambulance charges':
        $scope.billingTabData.ambulanceCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Miscellaneous charges':
        $scope.billingTabData.miscellaneousCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        case'Package charges':
        $scope.billingTabData.packageCharges[index].MANUALDISALLOWANCEDETAILS=$scope.MANUALDISDATALINK;
        break;
        default:
        break;
    }
    
}
$scope.callBillingFunction=function(funcName,para2,para3){
    switch(funcName)
    {
        case'getAllServices':getAllServices();break;
        case'addRowToManualDisallowance':addRowToManualDisallowance();break;
        case'deleteRowFromManualDisallowance':deleteRowFromManualDisallowance(para2);break;
        case'copyManualDisallowanceData':copyManualDisallowanceData(para3,currentIndexOfParticularPanel);break;
        default: break;
    }
    
};

$scope.openModal=function(categorylevel1,detailName,index)
{
    currentIndexOfParticularPanel=index;
    switch(categorylevel1)
    {
        case 'roomAndNursingCharges':
        
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.roomAndNursingCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.roomAndNursingCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.roomAndNursingCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.roomAndNursingCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.roomAndNursingCharges[index].PROPORTIONATEDEDUCTIONDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.roomAndNursingCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.roomAndNursingCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.roomAndNursingCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Room and Nursing charges";
                    if($scope.billingTabData.roomAndNursingCharges[index].OVERRIDEDETAILS.OVERRIDE)
                    {
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    }
                    break;
                default:
                    break;
            }
        
            break;
        case 'ICUcharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.ICUcharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.ICUcharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.ICUcharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.ICUcharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.ICUcharges[index].PROPORTIONATEDEDUCTIONDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.ICUcharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.ICUcharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.ICUcharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="ICU charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'OTcharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.OTcharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.OTcharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.OTcharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.OTcharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.OTcharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.OTcharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.OTcharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.OTcharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="OT charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'medicinesAndConsumableCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.medicinesAndConsumableCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.medicinesAndConsumableCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Medicines and consumable charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'professionalFeesCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.professionalFeesCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.professionalFeesCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.professionalFeesCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.professionalFeesCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.professionalFeesCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.professionalFeesCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.professionalFeesCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.professionalFeesCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Professional fees charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'investigationCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.investigationCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.investigationCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.investigationCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.investigationCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.investigationCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.investigationCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.investigationCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.investigationCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Investigation charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'ambulanceCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.ambulanceCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.ambulanceCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.ambulanceCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.ambulanceCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.ambulanceCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.ambulanceCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.ambulanceCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.ambulanceCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Ambulance charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'miscellaneousCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.miscellaneousCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.miscellaneousCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.miscellaneousCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.miscellaneousCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.miscellaneousCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.miscellaneousCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.miscellaneousCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.miscellaneousCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Miscellaneous charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        case 'packageCharges':
    
            switch(detailName)
            {
                case 'InvoiceDetails':
                    $scope.INVOICEMODALDATALINK=$scope.billingTabData.packageCharges[index].INVOICEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#invoiceDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'TariffDetails':
                    $scope.TARIFFMODALDATALINK=$scope.billingTabData.packageCharges[index].TARIFFDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#tariffDetailsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ManualDisallowance':
                    if(!$scope.billingTabData.packageCharges[index].MANUALDISALLOWANCEDETAILS)
                    {
                        $scope.MANUALDISDATALINK={};
                        $scope.MANUALDISDATALINK.ROWS=[];
                        $scope.MANUALDISDATALINK.ROWS[0]={};
                    }
                    else
                    {
                        $scope.MANUALDISDATALINK=$scope.billingTabData.packageCharges[index].MANUALDISALLOWANCEDETAILS;
                    }
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#manualDisallowancesModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'ProportionateDeduction':
                    $scope.PROPORTIONATEDEDUCTIONDATALINK=$scope.billingTabData.packageCharges[index].PROPORTIONATEDEDUCTION;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#proportionateDeductionModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'PerUnitPolicyLimits':
                    $scope.PERUNITPOLICYLIMITSDATALINK=$scope.billingTabData.packageCharges[index].PERUNITPOLICYLIMITSDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#perUnitPolicyLimitsModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'CoPay':
                    $scope.COPAYDATALINK=$scope.billingTabData.packageCharges[index].COPAYDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#coPayModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                case 'Override':
                    $scope.OVERRIDEDATALINK=$scope.billingTabData.packageCharges[index].OVERRIDEDETAILS;
                    $scope.CATEGORYLEVEL1OFMODAL="Package charges";
                    $('#overrideModal').modal({
                        backdrop: 'static'
                    });  
                    break;
                default:
                    break;
            }
        
            break;
        default:
            break;
    }
};


/*****************************************************billing details end****************************************/

        /*************************************************RESERVE TAB******************************************** */

        $scope.saveReserveData = function () {
            if (parseFloat($scope.reserveData.INDEMNITYRESERVEAMOUNT) <= 0) {
                $scope.showWarningMessage('Indemnity amount should be greater than zero');
                return;
            }
            if (!!$scope.reserveData.EXPENSERESERVEAMOUNT && parseFloat($scope.reserveData.EXPENSERESERVEAMOUNT) <= 0) {
                $scope.showWarningMessage('Expense amount should be greater than zero');
                return;
            }
            var input = {
                "updatedReserveData": $scope.reserveData,
                "ClaimInfo": $scope.ClaimInfo
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.decServiceCallCounter();
                    $('#ReserveAnchorTag').get(0).click();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in reserve creation");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addOrUpdateReserve(serviceArgs);
        }


        /*************************Approval History Data********************/
        $scope.getApprovalHistoryData = function (APPROVALNAME) {
            var input = {
                "FEATURESUMMARYID": $scope.claimSummary.FEATURESUMMARYID,
                "FEATURENUMBER": $scope.claimSummary.FEATURENUMBER,
                "APPROVALNAME": APPROVALNAME
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    switch (APPROVALNAME) {
                        case 'RESERVE':
                            if (data.ApprovalHistoryData.items.length > 0)
                                $scope.ReserveApprovalHistoryData = data.ApprovalHistoryData.items;
                            else
                                $scope.ReserveApprovalHistoryData = {};
                            $('#ReserveApprovalHistoryModal').modal('show');
                            break;
                        default:
                            break;
                    }

                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while getting reserve aprroval history data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getApprovalHistoryData(serviceArgs);
        }



        /*****************************************Medical Approval **********************************/
        /************************************Medical Approval History*******************************/
        $scope.openDeletePastHistoryModal = function (data,index) {
            $scope.dieseseNameToBeDeleted = data.DISEASENAME;
            $scope.dieseseIDToBeDeleted = data.HEALTHMEDICALPASTHISTORYID;
            $scope.dataToBePoped = index;
            $("#DeleteHistoryConfModal").modal('show');
        }

        $scope.deleteDiseaseFromDb = function () {
            var input = {
                "HEALTHMEDICALPASTHISTORYID": $scope.dieseseIDToBeDeleted
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("data successfully deleted");
                    //$scope.getTabData('MEDICALAPPROVAL');
                    $scope.medicalPastHistory.splice($scope.dataToBePoped,1);
                    $("#DeleteHistoryConfModal").modal('hide');
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while deleting past medical history data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.deleteDiseaseFromDb(serviceArgs);
        }

        $scope.AddMoreItemInPastHistory = function () {
            $scope.medicalPastHistory.push(
                {
                    "DISEASENAME": 'comorbidities/ailment',
                    "SELECTED": 'false',
                    "PERIODSINCE": null,
                    "DATESINCE": null
                }
            );
        }

        $scope.saveMedicalPastHistoryData = function () {
            if(!!$scope.form.medicalPastHistoryForm.$invalid){
                $scope.showWarningMessage('form invalid');
                $scope.form.medicalPastHistoryForm.submitted = true;
                return;
            }else{
                $scope.form.medicalPastHistoryForm.submitted = false;
            }
            for(var i=0;i<$scope.medicalPastHistory.length;i++){
                delete $scope.medicalPastHistory[i]["$$hashKey"];
                $scope.medicalPastHistory[i].DATESINCE = makeSQLServerComp($scope.medicalPastHistory[i].DATESINCE); 
            }
            
            var input = {
                "MEDICALPASTHISTORY": $scope.medicalPastHistory,
                "FEATURESUMMARYID" : $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("data added/updated successfully");
                    $scope.medicalPastHistory = data.MedicalPastHistoryData.items;
                    for(var i=0;i<$scope.medicalPastHistory.length;i++){
                        $scope.medicalPastHistory[i].DATESINCE = formatDateToDatePicker($scope.medicalPastHistory[i].DATESINCE); 
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    for(var i=0;i<$scope.medicalPastHistory.length;i++){
                        $scope.medicalPastHistory[i].DATESINCE = makePickerComp($scope.medicalPastHistory[i].DATESINCE); 
                    }
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while adding/updating past medical history data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveMedicalPastHistoryData(serviceArgs);
        }
        /**************************************Medical Adjudication******************************** */
        $scope.saveMedicalAdjudication =  function(NextTeamName){

            // var input = {
            //     "": $scope.dieseseIDToBeDeleted
            // }
            var serviceArgs = {
                //params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("data successfully saved");
                    
                    $scope.triggerBoundaryEvent(NextTeamName);

                    if(!!NextTeamName){
                        $scope.context.options.redirectTo.boundObject.redirectTo = NextTeamName;
                        $scope.context.trigger();
                    }

                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while adding medical adjudication data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveMedicalAdjudication(serviceArgs);
        }

        $scope.openMedicalCheckListandWarning = function(TAGNAME){
                    if(TAGNAME == 'CHECKLIST'){
                        $("#medicalCheckListModal").modal('show');
                    }else{
                        $("#warningListModal").modal('show');  
                    }
        }

        $scope.saveMedicalCheckLIst = function(){

            if(!!$scope.form.medicalCheckListForm.$invalid){
                $scope.showWarningMessage('form invalid');
                $scope.form.medicalCheckListForm.submitted = true;
                return;
            }else{
                $scope.form.medicalCheckListForm.submitted = false;
            }
            for(var i=0;i<$scope.medicalCheckList.length;i++){
                delete $scope.medicalCheckList[i]["$$hashKey"];
            }
            

            var input = {
                "MEDICALCHECKLIST": $scope.medicalCheckList,
                "FEATURESUMMARYID" : $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("data successfully updated");
                    $("#medicalCheckListModal").modal('hide');
                    $scope.decServiceCallCounter();
                    //$scope.openMedicalCheckListandWarning('WARNINGLIST');
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while saving medical checklist");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveMedicalCheckLIst(serviceArgs);
        }


        $scope.saveMedicalWarningLIst = function(){
            var input = {
                "": $scope.dieseseIDToBeDeleted
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("data successfully updated");
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while saving medical warninglist");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.saveMedicalWarningLIst(serviceArgs);
        }
        $scope.openNonDisclousureModal = function(){
            var input = {
                "FEATURESUMMARYID" : $scope.ClaimInfo.FEATURESUMMARYID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $("#nonDisclosureConditionModal").modal('show');
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while getting non diclosure data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getNonDisclosureData(serviceArgs);
        }

        $scope.submitNonDisclosure = function(){

            if(!!$scope.form.nonDisclosureForm.$invalid){
                $scope.showWarningMessage('form invalid');
                $scope.form.nonDisclosureForm.submitted = true;
                return;
            }else{
                $scope.form.nonDisclosureForm.submitted = false;
            }
            $scope.ClaimInfo.CLAIMNUMBER = $scope.claimSummary.CLAIMNUMBER;
            $scope.NONDISCLOSURE.FEATURESUMMARYID = $scope.ClaimInfo.FEATURESUMMARYID;
            var input = {
                "ClaimInfo": $scope.ClaimInfo,
                "NONDISCLOSURE":$scope.NONDISCLOSURE,
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $("#nonDisclosureConditionModal").modal('hide');
                    if(!!$scope.ClaimInfo.TASkNAME){
                        $scope.context.trigger();
                    }
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while adding non diclosure data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.submitNonDisclosure(serviceArgs);
        }

        $scope.triggerBoundaryEvent = function(){
            $scope.context.trigger();
        }

    }]);
