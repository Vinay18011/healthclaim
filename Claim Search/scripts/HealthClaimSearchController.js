angular.module("responsive.coaches")

    .directive('disableOnClick', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                // scope.isDesabled = false;
                element.bind('click', () => {
                    var text = element[0].innerText;
                    element[0].disabled = true;
                    element[0].innerText = 'Please Wait.....';
                    $timeout(function () {
                        element[0].disabled = false;
                        element[0].innerText = text;
                    }, 3000)
                });

            }

        }
    }])


    .directive('forAlphaNumericCapsSpace', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });

            }
        }
    })
    .directive('forAlphaNumeric', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue1 = inputValue.replace(/[^\w\s ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlphaNumericCaps', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive('forEmail', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s-@.]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive('forDateCalendar', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^0-9-]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive('forDigit', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\d]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forAddress', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s\-. ,/()]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlpha', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^a-z ]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })

    .directive('datePicker', function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                if (attrs.whichDate && (attrs.whichDate == "past")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                } else if (attrs.whichDate && (attrs.whichDate == "future")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                else if (attrs.whichDate && (attrs.whichDate == "beforePostmortom")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else if (attrs.whichDate && (attrs.whichDate == "afterDeath")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else {

                    var parent = $(element).parent().children('.input-group-addon');

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                $(element).on("dp.change", function (e) {



                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
                $(parent).on("dp.change", function (e) {
                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
            }
        };
    })

    .controller('HealthClaimSearchController', ['$scope', '$element', '$timeout', 'Coach', function ($scope, $element, $timeout, Coach) {
        "use strict";

        angular.extend($scope, new Coach($scope, $element, $timeout));

    

        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            })

        };

        $scope.decServiceCallCounter = function () {

            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });

        };

        $scope.showClaimIndex = true;

        $scope.firstNameCheck='Contains';
        $scope.lastNameCheck='Contains';

        $scope.searchClaim ={};

        $scope.showTable = false;

        $scope.getClaimSearch = function () {

            $scope.pageSize = 5;

            var input = {

                "claimSearchParameters": $scope.searchClaim,
                "FirstNamePref": $scope.firstNameCheck,
                "LastNamePref": $scope.lastNameCheck,
                "position": 0,
                "rows": $scope.pageSize

            };

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    $scope.showTable = true;

                    if ((!!data.ClaimList) && (data.ClaimList.items.length > 0)) {
                        $scope.ClaimSearchList = data.ClaimList.items;
                       
                    }

                    else{
                        $scope.ClaimSearchList = [];
                      
                    }

                    if ((!!data.resultCount) && (data.resultCount.items.length > 0)) {
                        $scope.resultCount = data.resultCount.items[0].resultcount;
                    }

                    $scope.assignPagVariables();

                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while getting Bank Audit Trail");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getClaimSearch(serviceArgs);

        }


        $scope.triggerFunction = function () {


            //console.log(duplicateClaimObj);
         
            //console.log($scope.bindingValue);
            this.context.trigger(function () {
                //console.log("jQuery button boundary event handled"); 
            })
        }

        $scope.currentPosition = 1;
        $scope.resultCount = 0;
        $scope.totalPages = 0;
        $scope.pageSize = 5;

        $scope.assignPagVariables = function () {

            $scope.totalPages = Math.ceil($scope.resultCount / $scope.pageSize);

            switch ($scope.totalPages) {
                case 1:
                    $scope.previousPage = 0;
                    $scope.nextPage = 0;
                    $scope.middlePage = 1;
                    break;
                case 2:
                    if ($scope.currentPosition == 1) {
                        $scope.nextPage = 2;
                        $scope.previousPage = 0;

                    } else if ($scope.currentPosition == 2) {
                        $scope.previousPage = 1;
                        $scope.nextPage = 0;
                    }
                    $scope.middlePage = $scope.currentPosition;
                    break;
                default:
                    if ($scope.currentPosition == 1) {

                        $scope.previousPage = 1;
                        $scope.middlePage = $scope.currentPosition + 1;
                        $scope.nextPage = $scope.currentPosition + 2;

                    } else if ($scope.currentPosition == $scope.totalPages) {

                        $scope.previousPage = $scope.currentPosition - 2;
                        $scope.middlePage = $scope.currentPosition - 1;
                        $scope.nextPage = $scope.currentPosition;

                    } else {
                        $scope.previousPage = $scope.currentPosition - 1;
                        $scope.middlePage = $scope.currentPosition;
                        $scope.nextPage = $scope.currentPosition + 1;
                    }
            }

        };


        $scope.openClaimSummary = function(index){
            $scope.selectedClaim = $scope.ClaimSearchList[index];
            $scope.showClaimIndex = false;
            $scope.showClaimSummary = true;
        }

      
        $scope.getProviderData = function () {

            var input = {
                "multipleSearchInput": [{ SearchParam: 'PRN', searchText: $scope.providerSearch.PRN }, { SearchParam: 'FIRSTNAME', searchText: $scope.providerSearch.FIRSTNAME }, { SearchParam: 'PAN', searchText: $scope.providerSearch.PAN },
                { SearchParam: 'MOBILENUMBER', searchText: $scope.providerSearch.MOBILENUMBER },
                { SearchParam: 'REGISTRATIONNUMBER', searchText: $scope.providerSearch.REGISTRATIONNUMBER }, { SearchParam: 'CITY', searchText: $scope.providerSearch.CITY }, { SearchParam: 'REGISTEREDENTITY', searchText: $scope.REGISTEREDENTITYTYPE }],
                "tableName": 'HEALTHPROVIDERMANAGEMENTMASTER',
                "startPosition": 0,
                "orderBy": 'HEALTHPROVIDERMANAGEMENTMASTERID',
                "noOfRow": 7,
                "COLUMNS": '*'
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (data.searchResults.items.length > 0) {
                        $scope.providerSearchData = data.searchResults.items;
                    }
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMultipleSearch(serviceArgs);
        }

        $scope.selectedProvider = function(x){
            $scope.searchClaim.PROVIDERNAME = x.ENTITYNAME;
            $('#PRNSearchModal').modal('hide');
        }

        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.IFSCalertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        };

        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = true;
            $scope.warning = false;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        };

        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;
            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        };

        $scope.showWarningMessage = function (warningmsg) {
            if ($scope.IFSCalertModal == false || $scope.IFSCalertModal == undefined)
                $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = false;
            $scope.warning = true;
            $scope.customeError = false;
            $scope.errCode = "Warning!!";
            $scope.errDesc = warningmsg;

            // $scope.$apply();

            if ($scope.errDesc == "Upload Files Contains Harmful Files !!")
                $scope.$apply();

            alertTimeoutCallback(5000);

        };

        //date picker and conversion functions

        var makeSQLServerComp = function (date) {
            if (!date)
                return null;
            else {
                var pos = date.indexOf("-");
                if (pos == 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var makePickerComp = function (date) {
            if (!date) {
                return null;
            } else {
                var pos = date.indexOf("-");
                if (pos != 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var formatDateToDatePicker = function (date) {

            if (!date) {
                return null;
            }
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
            // return [year, day, month].join('-');
        };


       

    }]);
