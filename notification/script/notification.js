angular.module("responsive.coaches")
   
    .controller("NotificationController", ["$scope", "$http", "$window", "$element", "Coach", function ($scope, $http, $window, $element, Coach) {

        angular.extend($scope, new Coach($scope, $element));
        $scope.NOTIFICATION = $scope.context.options.NOTIFICATION.boundObject.NOTIFICATION;
        $scope.endUserTask = function () {
            var input = {
                "taskId": $scope.context.options.TASKID.boundObject.TASKID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    //$scope.decServiceCallCounter();
                    $scope.context.trigger(function () {
                        console.log("Task Ended");
                    });
                },
                error: function (e) {
                    //$scope.decServiceCallCounter();
                    console.log(e);
                }
            };
            //$scope.incServiceCallCounter();
            $scope.context.options.endUserTask(serviceArgs);
        }
    }]);