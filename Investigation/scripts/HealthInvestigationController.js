angular.module("responsive.coaches")



    .directive('disableOnClick', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                // scope.isDesabled = false;
                element.bind('click', () => {
                    var text = element[0].innerText;
                    element[0].disabled = true;
                    element[0].innerText = 'Please Wait.....';
                    $timeout(function () {
                        element[0].disabled = false;
                        element[0].innerText = text;
                    }, 3000)
                });
            }

        }
    }])
    .directive('forAlphaNumericCapsSpace', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });

            }
        }
    })
    .directive('forAlphaNumeric', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue1 = inputValue.replace(/[^\w\s ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlphaNumericCaps', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forEmail', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s-@.]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forDateCalendar', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^0-9-]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forDigit', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\d]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('forAddress', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\w\s\-. ,/()]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forAlpha', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^a-z ]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('datePicker', function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                if (attrs.whichDate && (attrs.whichDate == "past")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                } else if (attrs.whichDate && (attrs.whichDate == "future")) {
                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: moment(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });
                    var parent = $(element).parent().children('.input-group-addon');
                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                else if (attrs.whichDate && (attrs.whichDate == "beforePostmortom")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        maxDate: moment(),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else if (attrs.whichDate && (attrs.whichDate == "afterDeath")) {

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),

                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    var parent = $(element).parent();
                    var dtp = parent.datetimepicker({
                        format: "DD-MM-YYYY",
                        minDate: scope.deathDetails.DATEOFDEATH.split("-").reverse().join("-"),
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                }
                else {

                    var parent = $(element).parent().children('.input-group-addon');

                    $(element).datetimepicker({
                        format: 'DD-MM-YYYY',
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'bottom'
                        }
                    });

                    parent.click(function () {
                        $(element).data('DateTimePicker').toggle();
                    });
                }
                $(element).on("dp.change", function (e) {



                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
                $(parent).on("dp.change", function (e) {
                    if (!e.date) {
                        ngModelCtrl.$setViewValue(null);

                    } else {
                        ngModelCtrl.$setViewValue(moment(e.date).format("DD-MM-YYYY"));
                    }
                    scope.$apply();
                });
            }
        };
    })
    .directive('stringToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value, 10);
                });
            }
        };
    })
    .controller('HealthInvestigationController', ['$scope', '$element', '$timeout', 'Coach', '$window', function ($scope, $element, $timeout, Coach, $window) {
        "use strict";

        angular.extend($scope, new Coach($scope, $element, $timeout));

       // $scope.FEATURESUMMARYID = $scope.context.options.FEATURESUMMARYID.boundObject.FEATURESUMMARYID;

       // $scope.taskId = $scope.context.options.TASKID.boundObject.TASKID;

        $scope.Investigator = $scope.context.options.Investigator.boundObject.Investigator;

      

        $scope.dashboard = true;

        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            })

        };

        $scope.decServiceCallCounter = function () {

            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });

        };

        $scope.getInvestigationData = function (x) {
            $scope.requestID = x.HEALTHINVESTIGATIONID;
            var input = {
                "FEATURESUMMARYID": x.FEATURESUMMARYID,
                "InvestigationID": $scope.requestID
            }
            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    var d = new Date();

                    $scope.TODAY = formatDateToDatePicker(d);

                    if (data.data1.items.length > 0) {
                        $scope.INVESTIGATIONDETAILS = data.data1.items[0].indexedMap;
                        //$scope.INVESTIGATIONRESPONSE = $scope.INVESTIGATIONDETAILS;
                        if ($scope.INVESTIGATIONDETAILS.MAINREQUESTID == undefined || $scope.INVESTIGATIONDETAILS.MAINREQUESTID == null || $scope.INVESTIGATIONDETAILS.MAINREQUESTID == '') {
                            $scope.INVESTIGATIONRESPONSE.MAINREQUESTID = $scope.requestID;
                        }
                        else{
                            $scope.INVESTIGATIONRESPONSE.MAINREQUESTID = $scope.INVESTIGATIONDETAILS.MAINREQUESTID;
                        }
                        if($scope.Investigator == 'true'){
                        $scope.INVESTIGATIONRESPONSE.ASSIGNEENTID = $scope.INVESTIGATIONDETAILS.ASSIGNEENTID;
                        $scope.INVESTIGATIONRESPONSE.MEMBERINVESTIGATION = $scope.INVESTIGATIONDETAILS.MEMBERINVESTIGATION;
                        $scope.INVESTIGATIONRESPONSE.HOSPITALINVESTIGATION = $scope.INVESTIGATIONDETAILS.HOSPITALINVESTIGATION;
                        $scope.INVESTIGATIONRESPONSE.INVESTIGATIONTYPE = $scope.INVESTIGATIONDETAILS.INVESTIGATIONTYPE;
                        $scope.INVESTIGATIONRESPONSE.ASSIGNTO = $scope.INVESTIGATIONDETAILS.ASSIGNTO;
                        $scope.INVESTIGATIONRESPONSE.INVESTIGATORNAME = $scope.INVESTIGATIONDETAILS.INVESTIGATORNAME;
                        $scope.INVESTIGATIONRESPONSE.INVESTIGATORNTID = $scope.INVESTIGATIONDETAILS.INVESTIGATORNTID;
                        $scope.INVESTIGATIONRESPONSE.FEATURESUMMARYID = $scope.INVESTIGATIONDETAILS.FEATURESUMMARYID;
                        
                        /*$scope.INVESTIGATIONRESPONSE.HOSPITALIZATIONREASON = $scope.INVESTIGATIONDETAILS.HOSPITALIZATIONREASON ;
                         $scope.INVESTIGATIONRESPONSE.TREATMENTREASON = $scope.INVESTIGATIONDETAILS.TREATMENTREASON;
                        $scope.INVESTIGATIONRESPONSE.SYMPTOMSINVESTIGATOR = $scope.INVESTIGATIONDETAILS.SYMPTOMSINVESTIGATOR;
                        $scope.INVESTIGATIONRESPONSE.SYMPTOMSPATIENT = $scope.INVESTIGATIONDETAILS.SYMPTOMSPATIENT;
                        $scope.INVESTIGATIONRESPONSE.DIAGNOSTICTESTS = $scope.INVESTIGATIONDETAILS.DIAGNOSTICTESTS;
                        $scope.INVESTIGATIONRESPONSE.TESTIMPRESSION = $scope.INVESTIGATIONDETAILS.TESTIMPRESSION;
                        $scope.INVESTIGATIONRESPONSE.INVESTIGATOROPINION = $scope.INVESTIGATIONDETAILS.INVESTIGATOROPINION;
                         */
                     
                        }

                        else{

                            $scope.INVESTIGATIONRESPONSE = $scope.INVESTIGATIONDETAILS;
                            $scope.INVESTIGATIONRESPONSE.REMARKS = '';

                        }
                        
                        $scope.ASSIGNEEREMARKS = '';
                        $scope.ASSIGNEEREMARKS = $scope.INVESTIGATIONDETAILS.REMARKS;
                        

                       

                        if ($scope.Investigator == 'false') {
                            var d = new Date();
                            $scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE = d;
                            $scope.INVESTIGATIONRESPONSE.REPORTSUBMISSIONDATE = formatDateToDatePicker($scope.INVESTIGATIONDETAILS.REPORTSUBMISSIONDATE);
                        }
                        else{
                            var d = new Date();
                            $scope.INVESTIGATIONRESPONSE.REPORTSUBMISSIONDATE = formatDateToDatePicker(d);
                        }
                    }

                    if (data.data2.items.length > 0) {
                        $scope.CLAIMDETAILS.PATIENTNAME = data.data2.items[0].indexedMap.FIRSTNAME;
                        $scope.CLAIMDETAILS.AGE = data.data2.items[0].indexedMap.AGE;
                        $scope.CLAIMDETAILS.GENDER = data.data2.items[0].indexedMap.GENDER;
                    }

                    if (data.data3.items.length > 0) {
                        $scope.CLAIMDETAILS.PROVIDERNAME = data.data3.items[0].indexedMap.PROVIDERNAME;
                        $scope.CLAIMDETAILS.CITY = data.data3.items[0].indexedMap.CITY;
                        $scope.CLAIMDETAILS.TREATINGDOCTOR = data.data3.items[0].indexedMap.FIRSTNAME;
                    }

                    if (data.data4.items.length > 0) {
                        $scope.CLAIMDETAILS.DATEOFADMISSIONLOSS = makeSQLServerComp(data.data4.items[0].indexedMap.DATEOFADMISSIONLOSS);
                        $scope.CLAIMDETAILS.DATEOFDISCHARGE = makeSQLServerComp(data.data4.items[0].indexedMap.DATEOFDISCHARGE);
                    }

                    if (!!data.data5.items) {
                        $scope.ICD10PCSDetails = [];
                        if (data.data5.items.length > 0) {
                            for (var i = 0; i < data.data5.items.length; i++) {
                                $scope.ICD10PCSDetails[i] = {};
                                $scope.ICD10PCSDetails[i] = data.data5.items[i].indexedMap;
                            }
                        }
                    }
                    if (data.data6.items.length > 0) {
                        $scope.CLAIMDETAILS.POLICYNUMBER = data.data3.items[0].indexedMap.POLICYNUMBER;
                        $scope.CLAIMDETAILS.MEMBERID = data.data3.items[0].indexedMap.MEMBERID;
                        $scope.CLAIMDETAILS.CLAIMNUMBER = data.data3.items[0].indexedMap.CLAIMNUMBER;
                    }

                

                    $scope.dashboard = false;


                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not get claim details!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getInvestigationData(serviceArgs);




        };

        $scope.saveInvestigation = function(){
            $scope.form.investigationForm.submitted = true;
            if (!$scope.form.investigationForm.$valid) {
                $scope.showWarningMessage('Enter mandatory data');
                return;
            }


            if($scope.Investigator == 'false'){
                if($scope.INVESTIGATIONRESPONSE.STATUS == 'REJECTED'){
                   /*  $scope.INVESTIGATIONRESPONSE.HOSPITALIZATIONREASON ='';
                    $scope.INVESTIGATIONRESPONSE.TREATMENTREASON = '';
                    $scope.INVESTIGATIONRESPONSE.SYMPTOMSINVESTIGATOR = '';
                    $scope.INVESTIGATIONRESPONSE.SYMPTOMSPATIENT = '';
                    $scope.INVESTIGATIONRESPONSE.DIAGNOSTICTESTS = '';
                    $scope.INVESTIGATIONRESPONSE.TESTIMPRESSION = '';
                    $scope.INVESTIGATIONRESPONSE.INVESTIGATOROPINION = '';
                    $scope.INVESTIGATIONRESPONSE. = '';
                    $scope.INVESTIGATIONRESPONSE. = ''; */

                    $scope.getMainRequestDetails();
                }
                else {
                    $scope.addUpdateInvestigation();
                }
            }
            else{
                $scope.addUpdateInvestigation();
            }

        }

        $scope.addUpdateInvestigation = function () {



            

            $scope.INVESTIGATIONRESPONSE.EXPECTEDSUBMISSIONDATE = makeSQLServerComp($scope.INVESTIGATIONRESPONSE.EXPECTEDSUBMISSIONDATE);

            $scope.INVESTIGATIONRESPONSE.REPORTSUBMISSIONDATE = makeSQLServerComp($scope.INVESTIGATIONRESPONSE.REPORTSUBMISSIONDATE);

            if ($scope.Investigator == 'false' && $scope.INVESTIGATIONRESPONSE.STATUS == 'REJECTED') {
                $scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE = new Date();
                $scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE = formatDateToDatePicker($scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE);
                $scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE = makeSQLServerComp($scope.INVESTIGATIONRESPONSE.APPOINTMENTDATE);
              
                $scope.INVESTIGATIONRESPONSE.REMARKS = $scope.INVESTIGATIONRESPONSE.REJECTIONREASON;
                
            }



            if ($scope.Investigator == 'true') {
                $scope.INVESTIGATIONRESPONSE.REQUESTRESPONSE = 'RESPONSE';
            }
            else {
                $scope.INVESTIGATIONRESPONSE.REQUESTRESPONSE = 'REQUEST';
            }

            var input = {

                "InvestigationRequest": $scope.INVESTIGATIONRESPONSE,
                "investigator": $scope.Investigator,
                "ID": $scope.requestID


            }

            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    debugger;

                    if ($scope.Investigator == 'true') {
                        if (!!$scope.INVESTIGATIONRESPONSE.ASSIGNEENTID) {
                           // $scope.context.options.assignTo.boundObject.assignTo = $scope.INVESTIGATIONRESPONSE.ASSIGNEENTID;
                        }
                    }

                    else {
                        //$scope.context.options.assignTo.boundObject.assignTo = $scope.INVESTIGATIONRESPONSE.INVESTIGATORNTID;
                        //$scope.context.options.acceptFlag.boundObject.acceptFlag = $scope.INVESTIGATIONRESPONSE.STATUS;
                    }

                    if (!!data.output1) {
                        if (data.output1.items.length > 0) {
                           // $scope.context.options.investigationID.boundObject.investigationID = data.output1.items[0].HEALTHINVESTIGATIONID;
                        }
                    }

                    $scope.dashboard = true;

                    $scope.showSuccessMessage('Request/Response Sent!');

                    $scope.context.trigger();
                    $scope.decServiceCallCounter();
                    $scope.$apply();
                },
                error: function (error) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(error, "Could not send request/response!");
                    $scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.addUpdateInvestigation(serviceArgs);


        };

        $scope.SPModalToggle = function () {
            if (!!$scope.INVESTIGATIONRESPONSE.ASSIGNTO) {
                $('#SPSearchModal').modal('toggle');
            }
            else {
                $scope.showWarningMessage('Select type of Service Provider!');
            }
        }

        $scope.reassignToChange = function () {
            var expected = new Date();
            expected.setDate(expected.getDate() + 7);
            $scope.INVESTIGATIONRESPONSE.EXPECTEDSUBMISSIONDATE = formatDateToDatePicker(expected);

            if ($scope.INVESTIGATIONRESPONSE.REASSIGNTO == 'Same investigator') {
                $scope.INVESTIGATIONRESPONSE.ASSIGNTO = $scope.INVESTIGATIONDETAILS.ASSIGNTO;
                $scope.INVESTIGATIONRESPONSE.TYPEOFSERVICEPROVIDER = $scope.INVESTIGATIONRESPONSE.TYPEOFSERVICEPROVIDER;
                $scope.INVESTIGATIONRESPONSE.INVESTIGATORNAME = $scope.INVESTIGATIONDETAILS.INVESTIGATORNAME;
                $scope.INVESTIGATIONRESPONSE.INVESTIGATORNTID = $scope.INVESTIGATIONDETAILS.INVESTIGATORNTID;



            }
            else {
                $scope.INVESTIGATIONRESPONSE.ASSIGNTO = '';
                $scope.INVESTIGATIONRESPONSE.TYPEOFSERVICEPROVIDER = '';
                $scope.INVESTIGATIONRESPONSE.INVESTIGATORNAME = '';
                $scope.INVESTIGATIONRESPONSE.INVESTIGATORNTID = '';
            }
        }

        $scope.getServiceProvider = function (pagStart) {

            if ($scope.INVESTIGATIONRESPONSE.ASSIGNTO == 'Internal Investigation Team') {
                var typeOfSP = 'Internal';
            }

            else {
                var typeOfSP = 'External';
            }



            var input = {
                "multipleSearchInput": [{ SearchParam: 'TYPE', searchText: typeOfSP }, { SearchParam: 'NAME', searchText: $scope.SPnameSearch }],
                "tableName": 'RESOURCEMASTER',
                "startPosition": (pagStart - 1) * 10,
                "orderBy": 'NAME',
                "noOfRow": 5,
                "COLUMNS": '*',

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (!!data.searchResults && data.searchResults.items.length > 0) {
                        $scope.ServiceProviderList = data.searchResults.items;
                    }
                    else {
                        $scope.ServiceProviderList = [];
                    }
                    if (!!data.RESULTCOUNT) {
                        $scope.SPresultCount = data.RESULTCOUNT;
                    }
                    $scope.assignPagVariables();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMultipleSearch(serviceArgs);
        }

        $scope.getInvestigationDashboard = function (pagStart,dataRequired) {

            if ($scope.Investigator == 'true'){
                var type = 'INVESTIGATOR';
            }
                else{
                var type = 'DOCTOR';
                    }

            var input = {
                
                "TYPE": type,
                "OFFSET": (pagStart - 1) * 10,
                "DATAREQUIRED": dataRequired,
                

            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    $scope.InvestigationData = [];
                    debugger;
                    if (!!data.data1 && data.data1.items.length > 0) {
                        for (var i = 0; i < data.data1.items.length;i++){
                            $scope.InvestigationData[i] = {};
                            $scope.InvestigationData[i] = data.data1.items[i].indexedMap;
                        }
                        
                    }
                    else {
                        $scope.InvestigationData = [];
                    }
                    if (!!data.data2 && data.data2.items.length > 0) {
                        $scope.resultCount = data.data2.items[0].indexedMap.RESULTCOUNT;
                    }
                    $scope.assignDashboardPagVariables();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while getting Dashboard Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getInvestigationDashboard(serviceArgs);
        }

        $scope.getInvestigationDashboard(1, 'PENDING');

        $scope.currentPosition = 1;
        $scope.resultCount = 0;
        $scope.totalPages = 0;
        $scope.pageSize = 2;

        $scope.assignDashboardPagVariables = function () {

            $scope.totalPages = Math.ceil($scope.resultCount / $scope.pageSize);

            switch ($scope.totalPages) {
                case 1:
                    $scope.previousPage = 0;
                    $scope.nextPage = 0;
                    $scope.middlePage = 1;
                    break;
                case 2:
                    if ($scope.currentPosition == 1) {
                        $scope.nextPage = 2;
                        $scope.previousPage = 0;

                    } else if ($scope.currentPosition == 2) {
                        $scope.previousPage = 1;
                        $scope.nextPage = 0;
                    }
                    $scope.middlePage = $scope.currentPosition;
                    break;
                default:
                    if ($scope.currentPosition == 1) {

                        $scope.previousPage = 1;
                        $scope.middlePage = $scope.currentPosition + 1;
                        $scope.nextPage = $scope.currentPosition + 2;

                    } else if ($scope.currentPosition == $scope.totalPages) {

                        $scope.previousPage = $scope.currentPosition - 2;
                        $scope.middlePage = $scope.currentPosition - 1;
                        $scope.nextPage = $scope.currentPosition;

                    } else {
                        $scope.previousPage = $scope.currentPosition - 1;
                        $scope.middlePage = $scope.currentPosition;
                        $scope.nextPage = $scope.currentPosition + 1;
                    }
            }

        };


        $scope.getMainRequestDetails = function () {

            var input = {
                "investigationID": $scope.INVESTIGATIONRESPONSE.MAINREQUESTID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    debugger;
                    if (!!data.MainRequestDetails && data.MainRequestDetails.items.length > 0) {
                        $scope.INVESTIGATIONRESPONSE.HOSPITALREGISTRATIONO = data.MainRequestDetails.items[0].HOSPITALREGISTRATIONO;
                        $scope.INVESTIGATIONRESPONSE.HOSPITALBILLAMOUNT = data.MainRequestDetails.items[0].HOSPITALBILLAMOUNT;
                        $scope.INVESTIGATIONRESPONSE.ROOMADMITTED = data.MainRequestDetails.items[0].ROOMADMITTED;
                        $scope.INVESTIGATIONRESPONSE.CROSSCHECKIPDREGISTER = data.MainRequestDetails.items[0].CROSSCHECKIPDREGISTER;
                        $scope.INVESTIGATIONRESPONSE.PATIENTBEDS = data.MainRequestDetails.items[0].PATIENTBEDS;
                        $scope.INVESTIGATIONRESPONSE.FINDINGS = data.MainRequestDetails.items[0].FINDINGS;
                        $scope.INVESTIGATIONRESPONSE.INDOORCASEPAPERS = data.MainRequestDetails.items[0].INDOORCASEPAPERS;
                        $scope.INVESTIGATIONRESPONSE.AGEOFPATIENT = data.MainRequestDetails.items[0].AGEOFPATIENT;
                        $scope.INVESTIGATIONRESPONSE.PASTHISTORYOFDISEASE = data.MainRequestDetails.items[0].PASTHISTORYOFDISEASE;
                        $scope.INVESTIGATIONRESPONSE.CHEIFCOMPLAINTS = data.MainRequestDetails.items[0].CHEIFCOMPLAINTS;
                        $scope.INVESTIGATIONRESPONSE.PATIENTREPORTS = data.MainRequestDetails.items[0].PATIENTREPORTS;
                      
                        
                    }
                    $scope.addUpdateInvestigation();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    debugger;
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while Searching Provider Data");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getMainRequestDetails(serviceArgs);
        }


        $scope.SPcurrentPosition = 1;
        $scope.SPresultCount = 0;
        $scope.SPtotalPages = 0;
        $scope.SPpageSize = 2;

        $scope.assignPagVariables = function () {

            $scope.SPtotalPages = Math.ceil($scope.SPresultCount / $scope.pageSize);

            switch ($scope.SPtotalPages) {
                case 1:
                    $scope.SPpreviousPage = 0;
                    $scope.SPnextPage = 0;
                    $scope.SPmiddlePage = 1;
                    break;
                case 2:
                    if ($scope.SPcurrentPosition == 1) {
                        $scope.SPnextPage = 2;
                        $scope.SPpreviousPage = 0;

                    } else if ($scope.SPcurrentPosition == 2) {
                        $scope.SPpreviousPage = 1;
                        $scope.SPnextPage = 0;
                    }
                    $scope.SPmiddlePage = $scope.SPcurrentPosition;
                    break;
                default:
                    if ($scope.SPcurrentPosition == 1) {

                        $scope.SPpreviousPage = 1;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition + 1;
                        $scope.SPnextPage = $scope.SPcurrentPosition + 2;

                    } else if ($scope.SPcurrentPosition == $scope.SPtotalPages) {

                        $scope.SPpreviousPage = $scope.SPcurrentPosition - 2;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition - 1;
                        $scope.SPnextPage = $scope.SPcurrentPosition;

                    } else {
                        $scope.SPpreviousPage = $scope.SPcurrentPosition - 1;
                        $scope.SPmiddlePage = $scope.SPcurrentPosition;
                        $scope.SPnextPage = $scope.SPcurrentPosition + 1;
                    }
            }

        };

        $scope.selectedSP = function (x) {
            $scope.INVESTIGATIONRESPONSE.INVESTIGATORNAME = x.NAME;
            $scope.INVESTIGATIONRESPONSE.INVESTIGATORNTID = x.USERID;

            $('#SPSearchModal').modal('toggle');
        }




        $scope.INVESTIGATIONDETAILS = {};
        $scope.CLAIMDETAILS = {};
        //$scope.getInvestigationData();
        $scope.INVESTIGATIONRESPONSE = {};

        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.IFSCalertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        };

        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = true;
            $scope.warning = false;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        };

        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;
            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        };

        $scope.showWarningMessage = function (warningmsg) {
            if ($scope.IFSCalertModal == false || $scope.IFSCalertModal == undefined)
                $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = false;
            $scope.warning = true;
            $scope.customeError = false;
            $scope.errCode = "Warning!!";
            $scope.errDesc = warningmsg;

            // $scope.$apply();

            if ($scope.errDesc == "Upload Files Contains Harmful Files !!")
                $scope.$apply();

            alertTimeoutCallback(5000);

        };

        //date picker and conversion functions

        var makeSQLServerComp = function (date) {
            if (!date)
                return null;
            else {
                var pos = date.indexOf("-");
                if (pos == 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var makePickerComp = function (date) {
            if (!date) {
                return null;
            } else {
                var pos = date.indexOf("-");
                if (pos != 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var formatDateToDatePicker = function (date) {

            if (!date) {
                return null;
            }
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
            // return [year, day, month].join('-');
        };





    }]);
