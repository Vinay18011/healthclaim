angular.module("responsive.coaches")

    .directive('forAlpha', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^a-z ]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        }
    })
    .directive('forDigit', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null)
                        return ''
                    cleanInputValue = inputValue.replace(/[^\d]/gi, '');
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind('keypress', function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .controller('InwardAssessorController', ['$scope', '$element', '$timeout', 'Coach', function ($scope, $element, $timeout, Coach) {
        "use strict";

        angular.extend($scope, new Coach($scope, $element, $timeout));
        $scope.isSubmit = false;


        $scope.TEAMNAME = $scope.context.options.TEAMNAME.boundObject.TEAMNAME;
        var start;
        var alertTimeoutCallback = function (time) {

            start = Date.now();
            $timeout(function () {

                var end = Date.now();

                if ((end - start) / 1000 > 4) {

                    $('.alert').animate({
                        opacity: 0
                    }, 500, function () {

                        $timeout(function () {

                            $scope.alertModal = false;
                            $scope.IFSCalertModal = false;
                            $scope.$apply(function () {
                                $('.alert').css({
                                    'opacity': 1
                                });
                            });
                        }, 50);

                    });
                }

            }, time);
        };

        $scope.incServiceCallCounter = function () {
            $scope.$evalAsync(function () {
                $scope.serviceCallCount = 0;
                $scope.serviceCallCount++;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount > 0) {
                    $("#cover").show();
                }
            })

        };

        $scope.decServiceCallCounter = function () {

            $scope.$evalAsync(function () {
                $scope.serviceCallCount--;
                console.log($scope.serviceCallCount);
                if ($scope.serviceCallCount <= 0) {
                    $("#cover").hide();
                }
            });
            
        };
        
        $scope.showSuccessMessage = function (customMessage) {
            $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = true;
            $scope.warning = false;
            $scope.errCode = "Success!!";
            $scope.errDesc = customMessage;

            $scope.$apply();

            alertTimeoutCallback(5000);
        };

        $scope.showErrorMessage = function (e, customMessage) {

            $scope.alertModal = true;
            $scope.alertModalError = true;
            $scope.error = true;
            $scope.success = false;
            $scope.warning = false;
            $scope.errCode = "Error!!";
            $scope.errDesc = customMessage;
            $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;

            $scope.$apply();

            alertTimeoutCallback(12000);
        };

        $scope.showWarningMessage = function (warningmsg) {
            if ($scope.IFSCalertModal == false || $scope.IFSCalertModal ==undefined)
                $scope.alertModal = true;
            $scope.alertModalError = false;
            $scope.error = false;
            $scope.success = false;
            $scope.warning = true;
            $scope.customeError = false;
            $scope.errCode = "Warning!!";
            $scope.errDesc = warningmsg;

           // $scope.$apply();

            if ($scope.errDesc == "Upload Files Contains Harmful Files !!")
                $scope.$apply();

            alertTimeoutCallback(5000);

        };

        //date picker and conversion functions

        var makeSQLServerComp = function (date) {
            if (!date)
                return null;
            else {
                var pos = date.indexOf("-");
                if (pos == 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var makePickerComp = function (date) {
            if (!date) {
                return null;
            } else {
                var pos = date.indexOf("-");
                if (pos != 2) {
                    return date.split("-").reverse().join("-");
                } else {
                    return date;
                }
            }
        };

        var formatDateToDatePicker = function (date) {

            if (!date) {
                return null;
            }
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
            // return [year, day, month].join('-');
        };

        
        $scope.CLAIMTYPE = [];
        $scope.CLAIMSUBTYPE = [];
        $scope.DOCTYPE = ['html','html2'];

        $scope.claimTypeData;
        $scope.inwardData;
        $scope.documentData;


        var populateClaimType = function (){
            $scope.claimTypeData.forEach(element => {
                if (!$scope.CLAIMTYPE.includes(element.HEALTHCLAIMTYPE)) {
                    $scope.CLAIMTYPE.push(element.HEALTHCLAIMTYPE);
                }
            })
            $scope.CLAIMTYPE.splice($scope.CLAIMTYPE.indexOf('Pre-Auth'),1);
            $scope.populateClaimSubType();
        };

        $scope.populateClaimSubType = function (){
            
            $scope.incServiceCallCounter();
            $scope.CLAIMSUBTYPE=[];
            if($scope.inwardData.CLAIMTYPE != undefined)
            {
                $scope.claimTypeData.forEach( element => {
                    if(element.HEALTHCLAIMTYPE == $scope.inwardData.CLAIMTYPE &&
                        !$scope.CLAIMSUBTYPE.includes(element.HEALTHCLAIMSUBTYPE)) {
                            $scope.CLAIMSUBTYPE.push(element.HEALTHCLAIMSUBTYPE);
                    }
                })
            }
            
            if($scope.dataEntryValidationForm.$pristine){
                return;
            }
            $scope.inwardData.CLAIMSUBTYPE = undefined;
            $scope.decServiceCallCounter();

        };

        $scope.ClaimID ='';
        $scope.getClaimDetails = function () {
            if ($scope.ClaimID.length < 9){
            $scope.showWarningMessage('Enter Valid Policy Number');
            return;
            }
            var input = {
                "searchText": $scope.ClaimID,
                "SearchParam": "CLAIMNUMBER",
                "tableName": "HEALTHFEATURESUMMARY",
                "startPosition": 0,
                "orderBy": "CLAIMNUMBER",
                "noOfRow": 5
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                    if (data.searchResults.items.length > 0) {
                        $scope.ClaimNumberlist = data.searchResults.items;
                    }
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error while featching Claim details");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getSearchData(serviceArgs);
        }

        
        var getInwardDetailsServiceCall = function () {

            var input = {
                "inwardId": $scope.context.options.INWARDID.boundObject.INWARDID
            };
            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    $scope.inwardData = data.inwardResults.items[0];
                    $scope.documentData = data.documentResults.items;
                    $scope.claimTypeData = data.claimTypeResults.items;

                    $scope.inwardData.CONSIGNMENTRECDATE = formatDateToDatePicker($scope.inwardData.CONSIGNMENTRECDATE);

                    populateClaimType();
                    $scope.$apply();
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                }
            };

            $scope.incServiceCallCounter();
            $scope.context.options.getInwardDetailsService(serviceArgs);
        };

        $scope.inwardData= {};
        $scope.updateInwardDetailsServiceCall = function (STATUS) {

            if(STATUS == 'REJECT')
            {
                if(!$scope.dataEntryValidationForm.remarks.$modelValue){
                    $scope.remarksShow = true;
                    $scope.showWarningMessage("Remarks mandatory for Rejecting.");
                    return;
                }
            }

            if($scope.dataEntryValidationForm.$invalid){
                $scope.dataEntryValidationForm.$error.required.forEach(element => { 
                    element.$setTouched();
                });
                $scope.showWarningMessage("Invalid Form Entry.");
                return;
            }
            
            $scope.documentData.forEach(element => {
                delete element['@metadata'];
                delete element['$$hashKey'];
            });

            $scope.inwardData.CONSIGNMENTRECDATE = makeSQLServerComp($scope.inwardData.CONSIGNMENTRECDATE);

            var input = {
                "inwardDetails":  $scope.inwardData,
                "status" : STATUS,
                "DOCUMENTLIST": $scope.documentData
            };
            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {

                    $scope.inwardData.CONSIGNMENTRECDATE = makePickerComp($scope.inwardData.CONSIGNMENTRECDATE);
                    $scope.showSuccessMessage("Saved");
                    if(STATUS=='SUBMIT' || STATUS=='ASSIGNTOSUPERVISOR' || STATUS == 'REJECT'){
                        $scope.endUserTask();
                    }
                    $scope.decServiceCallCounter();
                },
                error: function (e) {

                    $scope.inwardData.CONSIGNMENTRECDATE = makePickerComp($scope.inwardData.CONSIGNMENTRECDATE);
                    $scope.showErrorMessage(e,"Error while saving");
                    $scope.decServiceCallCounter();
                }
            };

            $scope.incServiceCallCounter();
            $scope.context.options.updateInwardDetailsService(serviceArgs);
        };

        $scope.showDeleteConfirmation = function(document) {
            $scope.deletedDocument = document;
            $("#deleteDocumentModal").modal('show');
        };

        $scope.deleteDocumentServiceCall = function(document) {
            
            $scope.documentData.splice($scope.documentData.indexOf(document),1);
            $scope.inwardData.NUMBEROFDOCUMENT = $scope.inwardData.NUMBEROFDOCUMENT-1;

            var input = {
                "documentId": document.HEALTHDOCUMENTMANAGEMENTID,
                "inwardId": document.INWARDID
            };
            var serviceArgs = {

                params: JSON.stringify(input),
                load: function (data) {
                    $scope.showSuccessMessage("Document deleted");
                    $scope.decServiceCallCounter();
                },
                error: function (e) {
                    $scope.showErrorMessage(e,"Error while deleting");
                    $scope.decServiceCallCounter();
                }
            };

            $scope.incServiceCallCounter();
            $scope.context.options.deleteDocumentDetailsService(serviceArgs);

        };

        getInwardDetailsServiceCall();

        
        var isLimitExceeded = function (file) {

            var size = file.size;
            size /= 1024 * 1024;
            if (size > 3)
                return true;
            return false;
        }

        $scope.setClaim = function(claim){
            $("#ClaimSearchID").modal('hide');
            if ($scope.inwardData == undefined){
                $scope.inwardData = {};
            }
            
            $scope.inwardData.CLAIMNUMBER = claim.CLAIMNUMBER;
        }

        $scope.uploadFile = function () {
            
            $scope.FileHexcode = "";
            var file = $('#file_upload').prop('files')[0];

            if(!$scope.allowedFileTypes){

                $scope.allowedFileTypes = "application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip,application/vnd.ms-excel,application/pdf,image/jpeg,image/jpg,text/plain,application/excel,application/x-excel,application/x-msexcel,image/pjpeg,image/gif,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation";
            }
            $("#addDocumentModal").modal('hide');
            if(!$scope.allowedFileTypes.includes(file.type.toString())){
                $scope.showWarningMessage('Upload Files Contains Harmful Files !!');
                return;
            }

            if (isLimitExceeded(file)) {
                $scope.showWarningMessage('File size limit is 3MB only');
                return;
            }
            $scope.incServiceCallCounter();
            var fileReader = new FileReader();
            fileReader.readAsArrayBuffer(file);
            fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            
            for(var i = 0; i < arr.length; i++) {
                $scope.FileHexcode += arr[i].toString(16);
            }
            $scope.decServiceCallCounter();
            $scope.uploadFileWithHexCode();
            };

        };

        
        $scope.uploadFileWithHexCode = function () {
            $scope.incServiceCallCounter();
            var file = $('#file_upload').prop('files')[0];
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {

               var resultString = reader.result.toString()
                   .slice(reader.result.indexOf('base64,') + 7);

               var contentType = reader.result.toString().slice(reader.result.indexOf(':') + 1,
                   reader.result.indexOf(';'));

               var input = {
                   "FileUploadInput": {
                       "FileName": file.name,
                       "FileContent": resultString,
                       "INWARDID": $scope.inwardData.INWARDID,
                       "FileType": "Bills",
                       "ContentType": contentType,
                   },
                   "FileHexcode": $scope.FileHexcode
               };

               var serviceArgs = {
                   params: JSON.stringify(input),
                   load: function (data) {

                       $scope.decServiceCallCounter();
                      
                       $scope.$apply();
                       $timeout(function () {
                           if(data.allowed){
                                getInwardDetailsServiceCall();
                            }else
                                $scope.showWarningMessage('Upload Files Contains Harmful Files !!');
                            
                        }, 500);
                   },

                   error: function (e) {
                       $scope.decServiceCallCounter();
                       $scope.showErrorMessage(e,"Erorr while calling uploadFileWithHexCode");
                   }
               };
               $scope.context.options.uploadFile(serviceArgs);
               $('#file_upload').val('');

           };
           reader.onerror = function (error) {
               $scope.showErrorMessage(error,'Error while Reading file' );
           };
        }

        $scope.downloadDocuments = function (HEALTHDOCUMENTMANAGEMENTID,OMNIDOCSID) {

            if(!!OMNIDOCSID){  
                $window.open("//www.google.com/");
                return;
            }

            var input = {
                "GetDocumentInput": {
                    "DocID": HEALTHDOCUMENTMANAGEMENTID,
                }
            }

            var serviceArgs = {
                params: JSON.stringify(input),

                load: function (data) {
                    $scope.decServiceCallCounter();

                    var fileContent = 'data:application/octet-stream;base64,' + data.GetDocumentOutput.FileContent;
                    $("#DownloadedDocument").attr('href', fileContent);
                    var downloadFileName = data.GetDocumentOutput.FileName;
                    $("#DownloadedDocument").attr('download', downloadFileName);
                    $("#DownloadedDocument").get(0).click();
                    
                },

                error: function (e) {
                    $scope.decServiceCallCounter();
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.getDocumentByID(serviceArgs);
        }

        $scope.endUserTask = function () {

            
            var input = {
                "taskId": $scope.context.options.TASKID.boundObject.TASKID
            }
            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {
                
                    $scope.decServiceCallCounter();
                    $scope.context.trigger();
                },
                error: function (e) {
                    $scope.decServiceCallCounter();
                    $scope.showErrorMessage(e, "Error in task ending");
                }
            };
            $scope.incServiceCallCounter();
            $scope.context.options.endUserTask(serviceArgs);
        }

    }]);
